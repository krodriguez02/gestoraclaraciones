class CargaProsasController < ApplicationController
  before_action :set_carga_prosa, only: [:show, :edit, :update, :destroy]

  @@byProsaUpload = "ProsaUpload"
  # GET /carga_prosas
  # GET /carga_prosas.json
  def index
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'ProsaUpload' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearProsaUpload = permisos.crear
          @editarProsaUpload = permisos.editar
          @leerProsaUpload = permisos.leer
          @eliminarProsaUpload = permisos.eliminar

          if permisos.view_name == @@byProsaUpload

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))

          @carga_prosas = CargaProsa.all
          @users = User.all

          @cinco = Date.today - 5.days
          #@carga_prosas = CargaProsa.where('created_at >= "' + @cinco.to_s + '"').order('created_at DESC')
          @archivos_creado = CargaProsa.where('created_at >= "' + @cinco.to_s + '"').order('created_at DESC')
          @carga_prosa = CargaProsa.new


          query = ""
          consulta = ""

          #----- Nombre -----
          if params[:searchN].present?
            name = params[:searchN].to_s
            nombre = name.squeeze '%' #<==
            uN = nombre.last

            if uN == '%'
              nombre = nombre.chomp("%")
            end

            @nombre = ""
            prN = nombre[0]

            if prN == '%'
              var1 = nombre.gsub(/^./, "")
              @nombre = var1
            else
              @nombre = nombre
            end

            consulta = "name_file_name = '#{@nombre.to_s}'"
            query = "#{t('views.archivos_index.name')}: " + @nombre.to_s
          end
          #----- Nombre -----

          #----- Fecha -----
          if params[:searchF].present?
            date = params[:searchF].to_s
            fecha = date.squeeze '%' #<==
            uF = fecha.last

            if uF == '%'
              fecha = fecha.chomp("%")
            end

            @fecha = ""
            prF = fecha[0]

            if prF == '%'
              var2 = nombre.gsub(/^./, "")
              @fecha = var2.gsub(/UTC/, "")
            else
              @fecha = fecha.gsub(/UTC/, "")
            end

            if consulta.bytesize > 0
              consulta += " and fecha like '%#{@fecha.chomp(" ")}%'"
              query += ", #{t('views.archivos_index.date')}: " + @fecha.to_s
            else
              consulta = "fecha like '%#{@fecha.chomp(" ")}%'"
              query = "#{t('views.archivos_index.date')}: " + @fecha.to_s
            end
          end
          #----- Fecha -----

          #----- Usuario -----
          if params[:searchU].present?
            usr = params[:searchU].to_s.sub(/\t/, '')

            if params[:searchU].to_s == "N/A" || params[:searchU].to_s == "n/a" || params[:searchU].to_s == "NA" || params[:searchU].to_s == "na"
              if consulta.bytesize > 0
                consulta += " and user_id is null"
                query += ", #{t('views.archivos_index.user')}: " + usr.to_s
              else
                consulta = "user_id is null"
                query = "#{t('views.archivos_index.user')}: " + usr.to_s
              end
            else

              @fi = params[:searchU]
              @user = User.find(params[:searchU])

              if consulta.bytesize > 0
                consulta += " and user_id = #{@fi}"
                query += ", #{t('views.archivos_index.user')}: " + @user.name.to_s + " " + @user.last_name.to_s
              else
                consulta = "user_id = #{@fi}"
                query = "#{t('views.archivos_index.user')}: " + @user.name.to_s + " " + @user.last_name.to_s
              end
            end

            @carga_prosas = CargaProsa.where(consulta).order(created_at: :desc).page(params[:page]).per(10)

            if @carga_prosas.present?
              @carga_prosas = CargaProsa.where(consulta).order(created_at: :desc).page(params[:page]).per(10)
              @query = query.to_s
            else

              @error = true
            end
          end
          #----- Usuario -----

          @carga_prosas = CargaProsa.all.page(params[:page]).per(10)

          if params[:reprocess].present?
            @file = CargaProsa.find(params[:id])
## -------------------- PARAMETRO REPROCESS --------------------
            puts 'Validar la existencia de la columna referencia 23 en reprocess'

#@table_names = ActiveRecord::Base.connection.tables
#names = "select TABLE_NAME as tabla from KMNADM_DEV.information_schema.tables" # 1 Obtener nombres de las tablas
            names = "select TABLE_NAME as tabla from kmgestorLiverpoolv4.information_schema.tables" # 1
            Timeout::timeout(0) {@table_names = ActiveRecord::Base::connection.exec_query(names)}
# puts 'columnas: ' + @table_names.columns.to_s

            tts = Array.new
            @table_names.each do |tbn|
              puts 'Tabla: ' + tbn['tabla'].to_s

              # Descomentar este codigo para tener 6 meses de historico con referencias de 23posiciones
              # if tbn[0..1] == "TT" && tbn[2..9].to_date >= 6.months.ago

              if tbn['tabla'][0..1] == "TT" && tbn['tabla'][2..9].to_date >= 3.months.ago
                tts.push(tbn['tabla'])
                variable = tts.push(tbn['tabla'])
                puts 'TTs: ' + variable.to_s

                puts 'Se obtienen los nombres de las tablas...'
                #col = "select * from KMNADM_DEV.information_schema.COLUMNS where TABLE_NAME = '#{tbn['tabla']}' and COLUMN_NAME = 'referencia23'" # 2 Se busca la columna referencia23
                col = "select * from kmgestorLiverpoolv4.information_schema.COLUMNS where TABLE_NAME = '#{tbn['tabla']}' and COLUMN_NAME = 'referencia23'"
                Timeout::timeout(0) {@col = ActiveRecord::Base::connection.exec_query(col)}
                # puts 'filas col: ' + @col.rows.to_s
                # puts 'columnas col: ' + @col.columns.to_s

                if !@col.present? # 3 Si la columna no existe se agrega
                  puts 'La columna referencia23 no existe, se va a agregar'
                  #add = "ALTER TABLE KMNADM_DEV.dbo.#{tbn['tabla']} ADD referencia23 integer;"
                  add = "ALTER TABLE kmgestorLiverpoolv4.dbo.#{tbn['tabla']} ADD referencia23 integer;"
                  Timeout::timeout(0) {@ad = ActiveRecord::Base::connection.exec_query(add)}

                  puts 'Se agregó la columna referencia 23'
                end

              end
            end

# ---------- HILO ----------
            Thread.new do
              puts cyan('Comienza procesamiento PROSA510')
              @prosa510 = @carga_prosa.name.path.to_s
              @namefile = @carga_prosa.name_file_name
              @user = current_user
              @idioma = params[:locale]
              filep = FileProcess.new
              filep.setInfo(@idioma, @prosa510, @namefile, @user, @idioma, tts)
              filep.processPROSA510
            end
            # ---------- HILO ----------
            ## -------------------- PARAMETRO REPROCESS --------------------
          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end


  end

  def archivo
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'ProsaUpload' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearProsaUpload = permisos.crear
          @editarProsaUpload = permisos.editar
          @leerProsaUpload = permisos.leer
          @eliminarProsaUpload = permisos.eliminar

          if permisos.view_name == @@byProsaUpload

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))

          @cinco = Date.today - 5.days
          @archives = Archive.where('created_at >= "' + @cinco.to_s + '"').order('created_at DESC')
          @archivos_creado = Archive.where('created_at >= "' + @cinco.to_s + '"').order('created_at DESC')
          render partial: 'carga_prosas/archivos'

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end


  end

  # GET /carga_prosas/1
  # GET /carga_prosas/1.json
  def show
  end

  # GET /carga_prosas/new
  def new
    @carga_prosa = CargaProsa.new
  end


  # GET /carga_prosas/1/edit
  def edit
  end

  # POST /carga_prosas
  # POST /carga_prosas.json
  def create
    if current_user
      @carga_prosa = CargaProsa.new(carga_prosa_params)
      hoy = Time.now
      hoy.strftime("%I:%M%p")

      user = current_user.id

      @carga_prosa.fecha = hoy.strftime("%H:%M:%S")
      @carga_prosa.user_id = user

      @idioma = params[:locale]
      respond_to do |format|
        if @carga_prosa.save
          puts 'Validar la existencia de la columna referencia 23'

          #@table_names = ActiveRecord::Base.connection.tables
          #names = "select TABLE_NAME as tabla from KMNADM_DEV.information_schema.tables" # 1
          names = "select TABLE_NAME as tabla from kmgestorLiverpoolv4.information_schema.tables" # 1
          Timeout::timeout(0) {@table_names = ActiveRecord::Base::connection.exec_query(names)}
          # puts 'columnas: ' + @table_names.columns.to_s

          tts = Array.new
          @table_names.each do |tbn|
            puts 'Tabla: ' + tbn['tabla'].to_s

            # Descomentar este codigo para tener 6 meses de historico con referencias de 23posiciones
            # if tbn[0..1] == "TT" && tbn[2..9].to_date >= 6.months.ago

            if tbn['tabla'][0..1] == "TT" && tbn['tabla'][2..9].to_date >= 3.months.ago
              tts.push(tbn['tabla'])
              variable = tts.push(tbn['tabla'])
              puts 'TTs: ' + variable.to_s

              puts 'Se obtienen los nombres de las tablas...'
              #col = "select * from KMNADM_DEV.information_schema.COLUMNS where TABLE_NAME = '#{tbn['tabla']}' and COLUMN_NAME = 'referencia23'"
              col = "select * from kmgestorLiverpoolv4.information_schema.COLUMNS where TABLE_NAME = '#{tbn['tabla']}' and COLUMN_NAME = 'referencia23'"
              Timeout::timeout(0) {@col = ActiveRecord::Base::connection.exec_query(col)}
              # puts 'filas col: ' + @col.rows.to_s
              # puts 'columnas col: ' + @col.columns.to_s

              if !@col.present?
                puts 'La columna referencia23 no existe, se va a agregar'
                #add = "ALTER TABLE KMNADM_DEV.dbo.#{tbn['tabla']} ADD referencia23 integer;"
                add = "ALTER TABLE kmgestorLiverpoolv4.dbo.#{tbn['tabla']} ADD referencia23 integer;"
                Timeout::timeout(0) {@ad = ActiveRecord::Base::connection.exec_query(add)}

                puts 'Se agregó la columna referencia 23'
              end

            end
          end

          # ---------- HILO ----------
          Thread.new do
            puts cyan('Comienza procesamiento PROSA510')
            @prosa510 = @carga_prosa.name.path.to_s
            @namefile = @carga_prosa.name_file_name
            @user = current_user
            @idioma = params[:locale]
            filep = FileProcess.new
            filep.setInfo(@idioma, @prosa510, @namefile, @user, @idioma, tts)
            filep.processPROSA510
          end
          # ---------- HILO ----------

          format.html {redirect_to carga_prosas_path, notice: t('views.al_created')}
          format.json {render :show, status: :created, location: @carga_prosa}
        else
          #format.html {render :new}
          format.html {render "home/index"}
          format.json {render json: @carga_prosa.errors, status: :unprocessable_entity}
        end
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # PATCH/PUT /carga_prosas/1
  # PATCH/PUT /carga_prosas/1.json
  def update
    if current_user
      respond_to do |format|
        if @carga_prosa.update(carga_prosa_params)
          format.html {redirect_to @carga_prosa, notice: 'Carga prosa was successfully updated.'}
          format.json {render :show, status: :ok, location: @carga_prosa}
        else
          format.html {render :edit}
          format.json {render json: @carga_prosa.errors, status: :unprocessable_entity}
        end
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # DELETE /carga_prosas/1
  # DELETE /carga_prosas/1.json
  def destroy
    if current_user

      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'ProsaUpload' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarProsaUpload = permisos.editar

          if permisos.view_name == @@byProsaUpload

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@eliminar == 1))

          @carga_prosa.destroy
          respond_to do |format|
            format.html {redirect_to carga_prosas_url, notice: t('views.al_des')}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end

  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_carga_prosa
    @carga_prosa = CargaProsa.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def carga_prosa_params
    params.require(:carga_prosa).permit(:name, :fecha, :user_id, :archivo)
  end
end
