class ClarificationTypesController < ApplicationController
  before_action :set_clarification_type, only: [:show, :edit, :update, :destroy]

  # GET /clarification_types
  # GET /clarification_types.json
  def index
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'ClarificationTypes' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearType = permisos.crear
          @editarType = permisos.editar
          @leerType = permisos.leer
          @eliminarType = permisos.eliminar

          if permisos.view_name == "ClarificationTypes"

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))
          @clarification_types = ClarificationType.where.not(:id => 1).page(params[:page]).per(10)

          if params[:search]
            @name = params[:clarif_name]

            @search= ClarificationType.where("name LIKE ?", "%#{@name}%").where.not(:id => 1).count
            @query = t('all.name') + ' = ' + @name

            if @search == 0
              @error = true
            end

            @clarification_types = ClarificationType.where("name LIKE ?", "%#{@name}%").where.not(:id => 1).page(params[:page]).per(10)
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /clarification_types/1
  # GET /clarification_types/1.json
  def show
  end

  # GET /clarification_types/new
  def new
    @clarification_type = ClarificationType.new
  end

  # GET /clarification_types/1/edit
  def edit
  end

  # POST /clarification_types
  # POST /clarification_types.json
  def create
    @clarification_type = ClarificationType.new(clarification_type_params)

    respond_to do |format|
      if @clarification_type.save
        format.html { redirect_to clarification_types_path, notice: t('views.clarif_type_index.creted') }
        format.json { render :show, status: :created, location: @clarification_type }
      else
        format.html { render :new }
        format.json { render json: clarification_types_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clarification_types/1
  # PATCH/PUT /clarification_types/1.json
  def update
    respond_to do |format|
      if @clarification_type.update(clarification_type_params)
        format.html { redirect_to clarification_types_path, notice: t('views.clarif_type_index.updated') }
        format.json { render :show, status: :ok, location: @clarification_type }
      else
        format.html { render :edit }
        format.json { render json: clarification_types_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clarification_types/1
  # DELETE /clarification_types/1.json
  def destroy
    @clarification_type.destroy

    @cl = Clarification.where(:clarification_type_id =>  @clarification_type.id)

    if @cl.length > 0
      @cl.each do |c|
        c.clarification_type_id = 1
        c.save
      end
    end
    respond_to do |format|
      format.html { redirect_to clarification_types_url, notice:  t('views.clarif_type_index.destroyed') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_clarification_type
      @clarification_type = ClarificationType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def clarification_type_params
      params.require(:clarification_type).permit(:name)
    end
end
