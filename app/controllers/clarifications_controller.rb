class ClarificationsController < ApplicationController
  before_action :set_clarification, only: [:show, :edit, :update, :destroy]

  @@byClarifications = "Clarifications"
  # GET /clarifications
  # GET /clarifications.json
  def index
    if params[:statement_id].present?
      id = params[:statement_id].to_i
      @clarif = Clarification.where('id = ?', id)
      respond_to do |format|
        format.html
        format.pdf do
          #pdf = ClarificationPdf.new(@clarif)
          @loc = params[:locale]
          pdf = ClarifPdf.new(@clarif, @loc) #<== ESTE ES EL NUEVO
          send_data pdf.render, filename: 'Clarification.pdf', type: 'application/pdf',
                    disposition: 'inline', size: '200'
        end
      end
    end

    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Clarifications' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearClarifications = permisos.crear
          @editarClarifications = permisos.editar
          @leerClarifications = permisos.leer
          @eliminarClarifications = permisos.eliminar

          if permisos.view_name == @@byClarifications

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))

          @user = User.all
          @states = State.all
          @types = ClarificationType.all
          @bins = Bin.all
          @clarifications = Clarification.all.page(params[:page]).per(10)

          if params[:search]
            @folio = params[:folio]
            @fecha = params[:date_hour]
            @fecha_end = params[:date_hour_end]
            @card_number = params[:card_number]
            @name = params[:name]
            @last_f = params[:last_f]
            @last_m = params[:last_m]
            @user_b = params[:user]
            @clar_type = params[:clar_type]
            @clar_bin = params[:clar_bin]
            @states_b = params[:states]

            query = ""
            @query = ""
            if @folio != ""
              if query == ""
                query = query + "folio = '#{@folio}'"
                @query = @query + "Folio = #{@folio}"
              else
                query = query + " and folio = '#{@folio}'"
                @query = @query + " #{t('views.home_index.y')} Folio = #{@folio}"
              end
            end

            if @fecha != "" && @fecha_end != ""
              if query == ""
                query = query + "created_at between '#{@fecha.to_date} 00:00:00.000' and '#{@fecha_end.to_date} 23:59:59.999'"
                @query = @query + "#{t('views.clarification_index.date_hour')} = #{@fecha.to_date} #{t('views.home_index.y')} #{@fecha_end.to_date}"
              else
                query = query + " and created_at between '#{@fecha.to_date} 00:00:00.000' and '#{@fecha_end.to_date} 23:59:59.999'"
                @query = @query + " #{t('views.home_index.y')} #{t('views.clarification_index.date_hour')} = #{@fecha.to_date} #{t('views.home_index.y')} #{@fecha_end.to_date}"
              end
            end

            if @card_number != ""
              if query == ""
                query = query + "card_number = '#{@card_number}'"
                @query = @query + "#{t('views.clarification_index.card_number')} = #{@card_number}"
              else
                query = query + " and card_number = '#{@card_number}'"
                @query = @query + " #{t('views.home_index.y')} #{t('views.clarification_index.card_number')} = #{@card_number}"
              end
            end

            if @name != ""
              if query == ""
                query = query + "client_name = '#{@name}'"
                @query = @query + "#{t('views.clarification_index.client_name')} = #{@name}"
              else
                query = query + " and client_name = '#{@name}'"
                @query = @query + " #{t('views.home_index.y')} #{t('views.clarification_index.client_name')} = #{@name}"
              end
            end

            if !@last_f.nil? && @last_f != ""
              if query == ""
                query = query + "last_name_f = '#{@last_f}'"
                @query = @query + "#{t('views.clarification_index.last_name_f')} = #{@last_f}"
              else
                query = query + " and last_name_f = '#{@last_f}'"
                @query = @query + " #{t('views.home_index.y')} #{t('views.clarification_index.last_name_f')} = #{@last_f}"
              end
            end

            if !@last_m.nil? && @last_m != ""
              if query == ""
                query = query + "last_name_m = '#{@last_m}'"
                @query = @query + "#{t('views.clarification_index.last_name_m')} = #{@last_m}"
              else
                query = query + " and last_name_m = '#{@last_m}'"
                @query = @query + " #{t('views.home_index.y')} #{t('views.clarification_index.last_name_m')} = #{@last_m}"
              end
            end

            if !@user_b.nil? && @user_b != ""
              if query == ""
                query = query + "user_id = '#{@user_b}'"
                @us = User.find(@user_b)
                @query = @query + "#{t('views.clarification_index.user')} = #{@us.name + ' ' + @us.last_name}"
              else
                query = query + " and user_id = '#{@user_b}'"
                @us = User.find(@user_b)
                @query = @query + " #{t('views.home_index.y')} #{t('views.clarification_index.user')} = #{@us.name + ' ' + @us.last_name}"
              end
            end

            if !@clar_type.nil? && @clar_type != ""
              if query == ""
                query = query + "clarification_type_id = '#{@clar_type}'"
                @tipo = ClarificationType.find(@clar_type)

                if @tipo.present? && !@tipo.nil?
                  @query = @query + "#{t('views.clarif_type_index.type')} = #{@tipo.name}"
                else
                  @query = @query + "#{t('views.clarif_type_index.type')} = #{@clar_type}"
                end

              else
                query = query + " and clarification_type_id = '#{@clar_type}'"
                @tipo = ClarificationType.find(@clar_type)

                if @tipo.present? && !@tipo.nil?
                  @query = @query + " #{t('views.home_index.y')} #{t('views.clarif_type_index.type')} = #{@tipo.name}"
                else
                  @query = @query + " #{t('views.home_index.y')} #{t('views.clarif_type_index.type')} = #{@clar_type}"
                end
              end
            end

            if !@clar_bin.nil? && @clar_bin != ""
              if @clar_bin == "other"
                @bins.each do |b|
                  if query == ""
                    query = query + "REPLACE(card_number, ' ', '') not like '#{b.bin}%'"
                  else
                    query = query + " and REPLACE(card_number, ' ', '') not like '#{b.bin}%'"
                  end
                end

                @query = @query + "#{t('views.clarif_type_index.bin')} = #{t('views.clarif_type_index.other')}"
              else
                if query == ""
                  @bin = Bin.find(@clar_bin)
                  query = query + "REPLACE(card_number, ' ', '') like '#{@bin.bin}%'"

                  if @bin.present? && !@bin.nil?
                    @query = @query + "#{t('views.clarif_type_index.bin')} = #{@bin.name} - #{@bin.bin}"
                  else
                    @query = @query + "#{t('views.clarif_type_index.bin')} = #{@clar_bin}"
                  end

                else
                  @bin = Bin.find(@clar_bin)
                  query = query + " and REPLACE(card_number, ' ', '') like '#{@bin.bin}%'"

                  if @bin.present? && !@bin.nil?
                    @query = @query + " #{t('views.home_index.y')} #{t('views.clarif_type_index.bin')} = #{@bin.name} - #{@bin.bin}"
                  else
                    @query = @query + " #{t('views.home_index.y')} #{t('views.clarif_type_index.bin')} = #{@clar_bin}"
                  end
                end
              end
            end

            if !@states_b.nil? && @states_b != ""
              if query == ""
                query = query + "state_id = '#{@states_b}'"
                @st = State.find_by_IdEstado(@states_b)
                @query = @query + "#{t('views.clarification_index.state')} = #{@st.Descripcion}"
              else
                query = query + " and state_id = '#{@states_b}'"
                @query = @query + " #{t('views.home_index.y')} #{t('views.clarification_index.state')} = #{@st.Descripcion}"
              end
            end

            @clarifications = Clarification.where(query).page(params[:page]).per(10)
            @xls_clarifications = Clarification.where(query)

            if params[:xls_g].present?
              @idioma = params[:idioma]

              if @idioma == "es"
                Thread.new do
                  puts 'Comienza hilo, idioma español'
                  create_xls = CreateXls.new
                  create_xls.setInfo(current_user.id, @xls_clarifications)
                  create_xls.clarificationsXlsEs
                end
              elsif @idioma == "en"
                Thread.new do
                  puts 'Comienza hilo, idioma ingles'
                  create_xls = CreateXls.new
                  create_xls.setInfo(current_user.id, @xls_clarifications)
                  create_xls.clarificationsXlsEn
                end
              end
            end
            if @clarifications.length == 0
              @error = true
            end

          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def dynamic

    @num_tarjeta = params[:numero_tarjeta]
    @search = false
    @nuevo_tt = false
    @edit_tt = false

    if !params[:numero_tarjeta].nil?
      @origen = params[:origen]
      if params[:id_acl]
        @id_acl = params[:id_acl]
      end
      @search = true
      if @origen == "new"
        @nuevo_tt = true
      elsif @origen == "edit"
        @selected = TransactionClarification.where(:idClarification => @id_acl)
        sels = ""
        @selected.each do |sel|
          sels = sels + sel.tableTTName.to_s + ',' + sel.idTransaction.to_s + '|'
        end

        @selected = sels
        @edit_tt = true
      end

      # ------------------------------------------------------------------------------------------------------------ PRUEBAS NUEVO QUERY YISHA
      @now = Time.now.to_time.to_s
      puts "--------------------* EMPIEZA #{@now} *--------------------"
      puts 'Se obtiene la fecha actual'

      hoy = Time.now.to_date.strftime('%Y%m%d') + '0101'
      @hoy = 'TT' + hoy
      puts 'Fecha Actual:'
      puts @hoy
      #seis = 3.months.ago.to_date.strftime('%Y%m%d') + '0101' #tres meses atrás
      seis = 6.months.ago.to_date.strftime('%Y%m%d') + '0101'
      @seis = 'TT' + seis
      puts 'Hace seis meses:'
      puts @seis

      #query1 = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '[#{@hoy}-#{@seis}]%'"
      query1 = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME >= '#{@seis}' AND TABLE_NAME < '#{@hoy}'"
      puts query1
      Timeout::timeout(0) {@tts = ActiveRecord::Base::connection.exec_query(query1)}
      puts 'Conteo de Tablas: '
      puts @tts.length
      cont = 0

      #ref = Array.new
      if !@tts.nil? && @tts.present?
        @tts.each do |n|
          #se va a formar un arreglo con las tts
          tts = Array.new

          @tn = n['TABLE_NAME']
          valor = {}
          valor[:tabla] = n['TABLE_NAME']
          tts.push(valor)

          # Solo  a las tablas que sean TT
          if @tn.start_with?('TT20')
            #if @tn.start_with?('sta')
            puts 'Tabla: '
            @bu = n['TABLE_NAME']
            puts cont += 1
            puts @bu

            col = "select * from #{@bu}"
            @col = ActiveRecord::Base::connection.exec_query(col)

            # Se busca que la columna referencia23 exista
            @ref23 = ActiveRecord::Base.connection.column_exists?(:id, @bu)
            puts 'Si es true es que si existe la columna referencia23: '
            puts @ref23
            puts 'El resultado de la búsqueda es: ' + @ref23.to_s

            # ---------- Prueba Loop
            transacciones = Array.new
            puts 'Se va a entrar al loop de prueba...'
            loop do
              tt = tts[0][:tabla].to_s.remove('TT').chomp('0101').to_date.strftime('%Y-%m-%d') # Aqui se obtiene la fecha
              if @ref23 == false
                puts '----------* No existe la columna *----------'
                #no = "Select Codigo_Aprov, Hora_Trans, Nombre_de_terminal, TERM_NAME_LOC, Monto1, Monto2 from kmgestorLiverpoolv4.dbo.#{tts[0][:tabla].to_s} where Numero_de_Tarjeta like '#{@num_tarjeta}%'"
                no = "Select Codigo_Aprov, Hora_Trans, Nombre_de_terminal, TERM_NAME_LOC, Monto1, Monto2 from #{tts[0][:tabla].to_s} where Numero_de_Tarjeta like '#{@num_tarjeta}%'"
                @no = ActiveRecord::Base::connection.exec_query(no)
                puts @no.length
                puts @no.nil?
                puts @no.present?
                puts no

                if @no.present? && !@no.nil?

                  @no.each do |no|
                    registro = {}
                    registro[:fecha] = tt
                    registro[:hora] = no['Hora_Trans']
                    registro[:num_tar] = @num_tarjeta
                    registro[:ref23] = 'N/A'
                    if no['Nombre_de_terminal'][0..8] == "005101530"
                      registro[:comercio] = no['TERM_NAME_LOC']
                    else
                      registro[:comercio] = no['Nombre_de_terminal']
                    end
                    registro[:monto1] = no['Monto1']
                    registro[:monto2] = no['Monto2']
                    registro[:id_tran] = no['Codigo_Aprov'].to_i
                    registro[:tt] = tts[0][:tabla].to_s
                    transacciones.push(registro)
                  end
                end


              else
                puts '----------* Si existe la columna *----------'
                #si = "Select Codigo_Aprov, Hora_Trans, referencia23, TERM_NAME_LOC, Nombre_de_terminal,Numero_de_Tarjeta, Monto1, Monto2 from kmgestorLiverpoolv4.dbo.#{tts[0][:tabla].to_s} where Numero_de_Tarjeta like '#{@num_tarjeta}%'"
                si = "Select Codigo_Aprov, Hora_Trans, referencia23, TERM_NAME_LOC, Nombre_de_terminal,Numero_de_Tarjeta, Monto1, Monto2 from #{tts[0][:tabla].to_s} where Numero_de_Tarjeta like '#{@num_tarjeta}%'"
                @consultaSi = ActiveRecord::Base::connection.exec_query(si)
                puts @consultaSi.length
                puts @consultaSi.nil?
                puts @consultaSi.present?
                puts si
                if @consultaSi.present? && !@consultaSi.nil?
                  @consultaSi.each do |si|
                    reg = {}
                    reg[:fecha] = tt
                    reg[:hora] = si['Hora_Trans']
                    reg[:num_tar] = @num_tarjeta
                    reg[:ref23] = si['referencia23']
                    if si['Nombre_de_terminal'][0..8] == "005101530"
                      reg[:comercio] = si['TERM_NAME_LOC']
                    else
                      reg[:comercio] = si['Nombre_de_terminal']
                    end
                    reg[:monto1] = si['Monto1']
                    reg[:monto2] = si['Monto2']
                    reg[:id_tran] = si['Codigo_Aprov'].to_i
                    reg[:tt] = tts[0][:tabla].to_s
                    transacciones.push(reg)
                  end
                end

              end #termina ref23 == true

              tts.delete_at(0)
              break if tts.length == 0

            end #termina loop
            # ---------- Prueba Loop
          end

        end
      end

      puts "--------------------* TERMINA #{@now} *--------------------"

      # ------------------------------------------------------------------------------------------------------------ PRUEBAS NUEVO QUERY YISHA

      # ---------------------** KARLA **---------------------
=begin
      # tables_query = "select TABLE_NAME FROM KMNADM_DEV.INFORMATION_SCHEMA.TABLES where TABLE_NAME like 'TT2%'"
      tables_query = "select TABLE_NAME FROM kmgestorLiverpoolv4.INFORMATION_SCHEMA.TABLES where TABLE_NAME like 'TT2%'"
      @table_names = ActiveRecord::Base::connection.exec_query(tables_query)

      # referencia = "select TABLE_NAME, COLUMN_NAME FROM KMNADM_DEV.INFORMATION_SCHEMA.COLUMNS where TABLE_NAME like 'TT2%' and COLUMN_NAME = 'referencia23'"
      referencia = "select TABLE_NAME FROM kmgestorLiverpoolv4.INFORMATION_SCHEMA.COLUMNS where TABLE_NAME like 'TT2%' and COLUMN_NAME = 'referencia23'"
      @referencia_23 = ActiveRecord::Base::connection.exec_query(referencia)

      refe = Array.new
      if !@referencia_23.nil? && @referencia_23.present?
        @referencia_23.each do |ref|
          refe.push(ref['TABLE_NAME'])
        end
      end

      puts refe.to_s
      tts = Array.new
      @table_names.each do |tbn|
        # Descomentar este codigo para mostrar 6 meses de histórico en las aclaraciones
        # if tbn['TABLE_NAME'][2..9].to_date >= 3.months.ago
        if tbn['TABLE_NAME'][2..9].to_date >= 3.months.ago
          valor = {}
          valor[:tabla] = tbn['TABLE_NAME']

          if (refe.include? tbn['TABLE_NAME'])
            valor[:referencia23] = true
          else
            valor[:referencia23] = false
          end
          tts.push(valor)
        end
      end


      transacciones = Array.new
      if tts.length > 0
        loop do

          tt = tts[0][:tabla].to_s.remove('TT').chomp('0101').to_date.strftime('%Y-%m-%d') # AQUI SE OBTIENE LA FECHA DEL REGISTRO
          if tts[0][:referencia23] == true
            # sql = "Select Codigo_Aprov, Hora_Trans, referencia23, TERM_NAME_LOC, Nombre_de_terminal, Numero_de_Tarjeta, Monto1, Monto2 from KMNADM_DEV.dbo.#{tts[0][:tabla].to_s} where Numero_de_Tarjeta like '#{@num_tarjeta}%'"
            sql = "Select Codigo_Aprov, Hora_Trans, referencia23, TERM_NAME_LOC, Nombre_de_terminal,Numero_de_Tarjeta, Monto1, Monto2 from kmgestorLiverpoolv4.dbo.#{tts[0][:tabla].to_s} where Numero_de_Tarjeta like '#{@num_tarjeta}%'"
            @consulta = ActiveRecord::Base::connection.exec_query(sql)
            puts sql

            if @consulta.present? && !@consulta.nil?

              @consulta.each do |con|
                registro = {}
                registro[:fecha] = tt
                registro[:hora] = con['Hora_Trans']
                registro[:num_tar] = @num_tarjeta
                registro[:ref23] = con['referencia23']
                if con['Nombre_de_terminal'][0..8] == "005101530"
                  registro[:comercio] = con['TERM_NAME_LOC']
                else
                  registro[:comercio] = con['Nombre_de_terminal']
                end
                registro[:monto1] = con['Monto1']
                registro[:monto2] = con['Monto2']
                registro[:id_tran] = con['Codigo_Aprov'].to_i
                registro[:tt] = tts[0][:tabla].to_s
                transacciones.push(registro)
              end
            end
          else
            # sql = "Select Codigo_Aprov, Hora_Trans, Nombre_de_terminal, TERM_NAME_LOC, Monto1, Monto2 from KMNADM_DEV.dbo.#{tts[0][:tabla].to_s} where Numero_de_Tarjeta like '#{@num_tarjeta}%'"
            sql = "Select Codigo_Aprov, Hora_Trans, Nombre_de_terminal, TERM_NAME_LOC, Monto1, Monto2 from kmgestorLiverpoolv4.dbo.#{tts[0][:tabla].to_s} where Numero_de_Tarjeta like '#{@num_tarjeta}%'"
            @consulta = ActiveRecord::Base::connection.exec_query(sql)
            puts sql

            if @consulta.present? && !@consulta.nil?

              @consulta.each do |con|
                registro = {}
                registro[:fecha] = tt
                registro[:hora] = con['Hora_Trans']
                registro[:num_tar] = @num_tarjeta
                registro[:ref23] = 'N/A'
                if con['Nombre_de_terminal'][0..8] == "005101530"
                  registro[:comercio] = con['TERM_NAME_LOC']
                else
                  registro[:comercio] = con['Nombre_de_terminal']
                end
                registro[:monto1] = con['Monto1']
                registro[:monto2] = con['Monto2']
                registro[:id_tran] = con['Codigo_Aprov'].to_i
                registro[:tt] = tts[0][:tabla].to_s
                transacciones.push(registro)
              end
            end
          end

          tts.delete_at(0)
          break if tts.length == 0
        end
      end
=end
      # ---------------------** KARLA **---------------------

      @transacciones = transacciones
      puts transacciones
      puts 'end'
    end

  rescue => e
    puts '----- Error -----'
    puts e.message
    logg = Logger.new('log/clarifications_log.log')
    logg.error('--------------------* Error Encontrado *--------------------')
    logger.error(e)
    puts '----- Error -----'


    if params[:id_acl]
      @clarification = Clarification.find(params[:id_acl])
    end
    render partial: 'dynamic'
  end

  # GET /clarifications/1
  # GET /clarifications/1.json
  def show
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Clarifications' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearClarifications = permisos.crear
          @editarClarifications = permisos.editar
          @leerClarifications = permisos.leer
          @eliminarClarifications = permisos.eliminar

          if permisos.view_name == @@byClarifications

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if (@@leer == 2)
          @trans = TransactionClarification.where(:idClarification => @clarification.id)
          @files_clarifications = FilesClarification.where(:idClarification => @clarification.id)
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /clarifications/new
  def new
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Clarifications' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarClarifications = permisos.editar

          if permisos.view_name == @@byClarifications

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8))
          @clarification = Clarification.new
          @folio = "ACL-" + Time.now.strftime('%Y%m%d-%H%M') + "-LIVER"
          @fecha_hora = Time.now.strftime('%Y-%m-%d %H:%M')
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /clarifications/1/edit
  def edit
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Clarifications' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarClarifications = permisos.editar

          if permisos.view_name == @@byClarifications

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@editar == 4))
          @states = State.all
          @types = ClarificationType.all

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # POST /clarifications
  # POST /clarifications.json
  def create
    if current_user
      @clarification = Clarification.new(clarification_params)
      @clarification.state_id = 1
      @clarification.user_id = current_user.id
      @clarification.save

      if @clarification.internal_clarification_type == 1 || @clarification.internal_clarification_type == "1"
        @clarification.atm_type = nil
        @clarification.purchase_commerce_type = nil
        @clarification.facts = nil
      elsif @clarification.internal_clarification_type == 2 || @clarification.internal_clarification_type == "2"
        @clarification.payment_date = nil
        @clarification.amount = nil
        @clarification.key_autorization = nil
        @clarification.bank = nil
        @clarification.purchase_commerce_type = nil
        @clarification.facts = nil
      elsif @clarification.internal_clarification_type == 3 || @clarification.internal_clarification_type == "3"
        @clarification.payment_date = nil
        @clarification.amount = nil
        @clarification.key_autorization = nil
        @clarification.bank = nil
        @clarification.atm_type = nil
      elsif @clarification.internal_clarification_type == 4 || @clarification.internal_clarification_type == "4"
        @clarification.payment_date = nil
        @clarification.amount = nil
        @clarification.key_autorization = nil
        @clarification.bank = nil
        @clarification.atm_type = nil
        @clarification.purchase_commerce_type = nil
        @clarification.facts = nil
      end

      @clarification.clarification_type_id = 1
      @clarification.created_at = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      @clarification.save
      if params[:selected] != ""
        @trans = params[:selected].split("|")

        @trans.each do |tran|
          @tran = tran.split(",")

          @tran_as = TransactionClarification.new
          @tran_as.idClarification = @clarification.id
          @tran_as.idTransaction = @tran[1]
          @tran_as.tableTTName = @tran[0]
          @tran_as.fecha_hora = @tran[2]
          @tran_as.numero_tarjeta = @tran[3]
          @tran_as.comercio = @tran[4]
          @tran_as.importe1 = @tran[5]
          @tran_as.importe2 = @tran[6]
          @tran_as.referencia23 = @tran[7]
          @tran_as.userMarked = current_user.id
          @tran_as.save

        end
      end

      puts 'SE VA A REDIRECCIONAR A FILES CLARIFICATIONS'
      id = @clarification.id
      redirect_to files_clarifications_path(:id => id)

      # respond_to do |format|
      #   if @clarification.save
      #     # format.html {redirect_to @clarification, notice: 'Clarification was successfully created.'}
      #     # format.json {render :show, status: :created, location: @clarification}
      #   else
      #     format.html {render :new}
      #     format.json {render json: @clarification.errors, status: :unprocessable_entity}
      #   end
      # end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # PATCH/PUT /clarifications/1
  # PATCH/PUT /clarifications/1.json
  def update
    if current_user
      @clarification.state_id = params[:clarification][:state_id]
      @clarification.clarification_type_id = params[:clarification][:clarification_type_id]
      @clarification.user_id = current_user.id
      @clarification.client_name = params[:clarification][:client_name]
      @clarification.last_name_f = params[:clarification][:last_name_f]
      @clarification.last_name_m = params[:clarification][:last_name_m]
      @clarification.card_number = params[:clarification][:card_number]
      @clarification.phone = params[:clarification][:phone]
      @clarification.email = params[:clarification][:email]
      @clarification.account_type = params[:clarification][:account_type]
      @clarification.internal_clarification_type = params[:clarification][:internal_clarification_type]

      if @clarification.internal_clarification_type == 1 || @clarification.internal_clarification_type == "1"
        @clarification.payment_date = params[:clarification][:payment_date]
        @clarification.amount = params[:clarification][:amount]
        @clarification.key_autorization = params[:clarification][:key_autorization]
        @clarification.bank = params[:clarification][:bank]
        @clarification.atm_type = nil
        @clarification.purchase_commerce_type = nil
        @clarification.facts = nil
      elsif @clarification.internal_clarification_type == 2 || @clarification.internal_clarification_type == "2"
        @clarification.payment_date = nil
        @clarification.amount = nil
        @clarification.key_autorization = nil
        @clarification.bank = nil
        @clarification.atm_type = params[:clarification][:atm_type]
        @clarification.purchase_commerce_type = nil
        @clarification.facts = nil
      elsif @clarification.internal_clarification_type == 3 || @clarification.internal_clarification_type == "3"
        @clarification.payment_date = nil
        @clarification.amount = nil
        @clarification.key_autorization = nil
        @clarification.bank = nil
        @clarification.atm_type = nil
        @clarification.purchase_commerce_type = params[:clarification][:purchase_commerce_type]
        @clarification.facts = params[:clarification][:facts]
      elsif @clarification.internal_clarification_type == 4 || @clarification.internal_clarification_type == "4"
        @clarification.payment_date = nil
        @clarification.amount = nil
        @clarification.key_autorization = nil
        @clarification.bank = nil
        @clarification.atm_type = nil
        @clarification.purchase_commerce_type = nil
        @clarification.facts = nil
      end
      @clarification.created_at = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      @clarification.save

      @trans = TransactionClarification.where(:idClarification => @clarification.id)

      @trans.each do |trans|
        trans.destroy
      end

      if params[:selected] != ""
        @trans = params[:selected].split("|")
        puts @trans.to_s

        @trans.each do |tran|
          @tran = tran.split(",")
          @tran_as = TransactionClarification.new
          @tran_as.idClarification = @clarification.id
          @tran_as.idTransaction = @tran[1]
          @tran_as.tableTTName = @tran[0]
          @tran_as.fecha_hora = @tran[2]
          @tran_as.numero_tarjeta = @tran[3]
          @tran_as.comercio = @tran[4]
          @tran_as.importe1 = @tran[5]
          @tran_as.importe2 = @tran[6]
          @tran_as.referencia23 = @tran[7]
          @tran_as.userMarked = current_user.id
          @tran_as.save

        end
      end

      id = @clarification.id
      redirect_to files_clarifications_path(:id => id)
      # respond_to do |format|
      #   if @clarification.update(clarification_params)
      #     format.html {redirect_to @clarification, notice: 'Clarification was successfully updated.'}
      #     format.json {render :show, status: :ok, location: @clarification}
      #   else
      #     format.html {render :edit}
      #     format.json {render json: @clarification.errors, status: :unprocessable_entity}
      #   end
      # end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # DELETE /clarifications/1
  # DELETE /clarifications/1.json
  def destroy
    if current_user

      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Clarifications' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarClarifications = permisos.editar

          if permisos.view_name == @@byClarifications

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@eliminar == 1))

          @clarification.destroy

          @trans = TransactionClarification.where(:idClarification => @clarification.id)

          if !@trans.nil? && @trans.present?
            @trans.each do |trans|
              trans.destroy
            end
          end

          @files = FilesClarification.where(:idClarification => @clarification.id)

          if !@files.nil? && @files.present?
            @files.each do |file|
              file.destroy
            end
          end

          respond_to do |format|
            format.html {redirect_to clarifications_url, notice: 'Clarification was successfully destroyed.'}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end

  end


  private

  # Use callbacks to share common setup or constraints between actions.
  def set_clarification
    @clarification = Clarification.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def clarification_params
    params.require(:clarification).permit(:client_name, :last_name_f, :last_name_m, :card_number, :phone, :celphone, :email, :account_type, :internal_clarification_type, :payment_date, :amount, :key_autorization, :bank, :atm_type, :purchase_commerce_type, :facts, :folio)
  end
end
