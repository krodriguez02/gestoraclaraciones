class FilesClarificationsController < ApplicationController
  before_action :set_files_clarification, only: [:show, :edit, :update, :destroy]

  # GET /files_clarifications
  # GET /files_clarifications.json
  def index

    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Clarifications' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearClarification = permisos.crear
          @editarClarification = permisos.editar
          @leerClarification = permisos.leer
          @eliminarClarification = permisos.eliminar

          if permisos.view_name == "Clarifications"

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))
          @idC = params[:id]
          #id = params[:id]
          #@clarif = Clarification.find(id)

          if params[:statement_id].present?
            id = params[:statement_id].to_i
            @clarif = Clarification.where('id = ?', id)
            respond_to do |format|
              format.html
              format.pdf do
                #pdf = ClarificationPdf.new(@clarif)
                pdf = ClarifPdf.new(@clarif) #<== ESTE ES EL NUEVO
                send_data pdf.render, filename: 'Clarification.pdf', type: 'application/pdf',
                          disposition: 'inline', size: '200'
              end
            end
          end
          @files_clarifications = FilesClarification.where(:idClarification => @idC)




          #end
          #@carga_prosas = CargaProsa.all
          @users = User.all

          @cinco = Date.today - 5.days
          #@carga_prosas = CargaProsa.where('created_at >= "' + @cinco.to_s + '"').order('created_at DESC')
          @archivos_creado = FilesClarification.where('created_at >= "' + @cinco.to_s + '"').order('created_at DESC')
          @files_clarification = FilesClarification.new


        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end


  end

  def archivo
    @cinco = Date.today - 5.days
    @archives = Archive.where('created_at >= "' + @cinco.to_s + '"').order('created_at DESC')
    @archivos_creado = Archive.where('created_at >= "' + @cinco.to_s + '"').order('created_at DESC')
    render partial: 'files_clarifications/archivos'
  end

  # GET /files_clarifications/1
  # GET /files_clarifications/1.json
  def show
  end

  # GET /files_clarifications/new
  def new
    @files_clarification = FilesClarification.new
    puts 'entre a new'
  end

  # GET /files_clarifications/1/edit
  def edit
  end

  # POST /files_clarifications
  # POST /files_clarifications.json
  def create

    @files_clarification = FilesClarification.new(files_clarification_params)
    user = current_user.id

    @files_clarification.user_id = user
    idClar = params[:idc]
    @idC = params[:idc]
    @files_clarification.idClarification = idClar
    valid = @files_clarification.idClarification = idClar

    puts 'se va a verificar los datos guardados ' + valid.to_s


    @files_clarification
    respond_to do |format|

      if @files_clarification.save
        format.html {redirect_to files_clarifications_path(id: @idC), notice: 'Clarification was successfully created.'}
        format.json {render :show, status: :created, location: @files_clarification}
        #format.json {render json: true}
      else
        format.html {render :new}
        format.json {render json: files_clarifications_url.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /files_clarifications/1
  # PATCH/PUT /files_clarifications/1.json
  def update
    respond_to do |format|
      if @files_clarification.update(files_clarification_params)
        format.html {redirect_to @files_clarification, notice: 'Files clarification was successfully updated.'}
        format.json {render :show, status: :ok, location: @files_clarification}
      else
        format.html {render :edit}
        format.json {render json: @files_clarification.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /files_clarifications/1
  # DELETE /files_clarifications/1.json
  def destroy
    @files_clarification.destroy
    respond_to do |format|
      format.html {redirect_to files_clarifications_url, notice: 'Files clarification was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_files_clarification
    @files_clarification = FilesClarification.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def files_clarification_params
    params.require(:files_clarification).permit(:name, :user_id, :idClarification)
  end
end
