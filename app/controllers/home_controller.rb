class HomeController < ApplicationController
  @@dias = 2
  #@@usuario = User.find(current_user.id);
  #def self.dynamic
  @@byHome = "Home"

  def index
    if current_user
      @page = "Areas"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "3"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      @date = Time.now
      # @description = General.all.count.to_i

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # @historic.description = @description
      # @historic.save


      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Home' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearHome = permisos.crear
          @editarHome = permisos.editar
          @leerHome = permisos.leer
          @eliminarHome = permisos.eliminar

          if permisos.view_name == @@byHome

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        @clarifications = Clarification.all.count

        if @clarifications > 0
          @sin_info = false

          #--------------------------------------------- codigo grafica lineas 1
          @ayer = Time.now - 24.hours
          periodo_minutos = Array.new
          counts1 = Array.new

          while (@ayer <= Time.now)
            periodo_minutos.push(@ayer.to_datetime.strftime('%Y-%m-%d %H:00:00'))

            @ayer = @ayer + 1.hour
          end

          sql = 'SELECT dateadd(hour, datediff(hour, 0, created_at), 0) as fecha_hora, Count(*) cantidad FROM clarifications GROUP BY dateadd(hour, datediff(hour, 0, created_at), 0) ORDER BY dateadd(hour, datediff(hour, 0, created_at), 0) desc;'
          @consulta1 = ActiveRecord::Base::connection.exec_query(sql)

          if @consulta1.present? && !@consulta1.nil?
            loop do
              @encontro = false
              @consulta1.each do |c|
                @fecha_hora = c['fecha_hora']
                @cant = c['cantidad']
                @comp = periodo_minutos[0].to_s + ' -0500'
                @hola = true
                if c['fecha_hora'] == @comp
                  reg = {}
                  reg[:fecha] = c['fecha_hora'].to_datetime.strftime('%Y-%m-%d %H:00:00')
                  reg[:cantidad] = c['cantidad']

                  counts1.push(reg)

                  @encontro = true
                end
              end

              if @encontro == false
                reg = {}
                reg[:fecha] = periodo_minutos[0]
                reg[:cantidad] = '0'

                counts1.push(reg)
              end

              periodo_minutos.delete_at(0)
              break if periodo_minutos.length == 0
            end
          else
            loop do
              reg = {}
              reg[:fecha] = periodo_minutos[0]
              reg[:cantidad] = '0'

              counts1.push(reg)

              periodo_minutos.delete_at(0)
              break if periodo_minutos.length == 0
            end
          end

          @graph1 = counts1

          #--------------------------------------------------------- Codigo grafica dona 1 POR ESTADO
          sql2 = "select st.Descripcion estado, count(cl.id) cantidad from clarifications cl full join states st on cl.state_id = st.IdEstado group by st.Descripcion"
          @consulta2 = ActiveRecord::Base::connection.exec_query(sql2)

          if @consulta2.present? && !@consulta2.nil?
            counts2 = Array.new

            @consulta2.each do |c|
              reg = {}
              reg[:estado] = c['estado']
              reg[:cantidad] = c['cantidad']

              counts2.push(reg)
            end

            @graph2 = counts2
          end

          #--------------------------------------------------------- Codigo grafica dona 2 POR TIPO DE ACLARACION
          sql3 = "select ct.name tipo, count(cl.id) cantidad from clarifications cl full join clarification_types ct on cl.clarification_type_id = ct.id group by ct.name"
          @consulta3 = ActiveRecord::Base::connection.exec_query(sql3)

          if @consulta3.present? && !@consulta3.nil?
            counts3 = Array.new

            @consulta3.each do |c|
              reg = {}
              reg[:tipo] = c['tipo']
              reg[:cantidad] = c['cantidad']

              counts3.push(reg)
            end

            @graph3 = counts3
          end

          #--------------------------------------------------------- Codigo grafica dona 3 POR BIN
          @bins = Bin.all
          sql_4_not_exist = ""
          counts4 = Array.new

          @bins.each do |b|
            if sql_4_not_exist == ""
              sql_4_not_exist = "select count(*) cantidad_n from clarifications where REPLACE(card_number, ' ', '') not like '#{b.bin}%'"
            else
              sql_4_not_exist = sql_4_not_exist + " and REPLACE(card_number, ' ', '') not like '#{b.bin}%'"
            end

            sql4 = "select count(*) cantidad from clarifications where REPLACE(card_number, ' ', '') like '#{b.bin}%'"
            @consulta4 = ActiveRecord::Base::connection.exec_query(sql4)

            if @consulta4.present? && !@consulta4.nil?
              @consulta4.each do |c|
                reg = {}
                reg[:tipo] = b.name + ', ' + b.bin
                reg[:cantidad] = c['cantidad']

                counts4.push(reg)
              end
            end
          end

          @consulta4 = ActiveRecord::Base::connection.exec_query(sql_4_not_exist)

          if @consulta4.present? && !@consulta4.nil?
            @consulta4.each do |c|
              reg = {}
              reg[:tipo] = 'Otro'
              reg[:cantidad] = c['cantidad_n']

              counts4.push(reg)
            end
          end

          @graph4 = counts4
        else
          @sin_info = true
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def par1
    @hora_ant = Time.now - 30
    @hora_ant = @hora_ant.to_date.to_formatted_s(:db)
    @hora_act = Time.now.to_date.to_formatted_s(:db)

    #Aclaraciones con estado no atendido
    @clar_ant = Clarification.where(:created_at => @hora_ant).where(:state_id => 1).count
    @clar_act = Clarification.where(:created_at => @hora_act).where(:state_id => 1).count

    if @clar_act > @clar_ant
      @mensaje = true
    else
      @mensaje = false
    end

    render :partial => "par1"
  end

  def par2
    @hora_ant = Time.now - 30
    @hora_ant = @hora_ant.to_date.to_formatted_s(:db)
    @hora_act = Time.now.to_date.to_formatted_s(:db)

    #Aclaraciones con estado diferente a no atendido
    @clar_ant = Clarification.where(:created_at => @hora_ant).where.not(:state_id => 1).count
    @clar_act = Clarification.where(:created_at => @hora_act).where.not(:state_id => 1).count

    if @clar_act > @clar_ant
      @mensaje = true
    else
      @mensaje = false
    end

    render :partial => "par2"
  end

  def par3
    @hora_ant = Time.now - 30
    @hora_ant = @hora_ant.to_date.to_formatted_s(:db)
    @hora_act = Time.now.to_date.to_formatted_s(:db)

    #Total de aclaraciones
    @clar_ant = Clarification.where(:created_at => @hora_ant).count
    @clar_act = Clarification.where(:created_at => @hora_act).count

    if @clar_act > @clar_ant
      @mensaje = true
    else
      @mensaje = false
    end

    render :partial => "par3"
  end

  def mensajeAlerta
    render :partial => "mensajeAlerta"
  end
end