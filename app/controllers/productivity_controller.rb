class ProductivityController < ApplicationController
  def index
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Productivity' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearType = permisos.crear
          @editarType = permisos.editar
          @leerType = permisos.leer
          @eliminarType = permisos.eliminar

          if permisos.view_name == "Productivity"

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@leer == 2))
          @clarifications = Clarification.all.count

          if @clarifications > 0
            @sin_info = false
            graph1 = Array.new
            @users = User.all

            if params[:search_for_users].present?
              @states = State.all
              @user = params[:users_states].to_i
              @user_busqueda = User.find(@user)
              @user_busqueda = @user_busqueda.name + ' ' + @user_busqueda.last_name
              @busqueda = true
              @states.each do |st|
                sql = "select st.Descripcion estado, count(*) cantidad from clarifications c join states st on c.state_id = st.IdEstado where user_id = #{@user} and c.state_id = #{st.id} group by st.Descripcion"
                @consulta1 = ActiveRecord::Base::connection.exec_query(sql)

                if @consulta1.present? && !@consulta1.nil?
                  valor = {}
                  valor[:label] = st.Descripcion
                  if @consulta1.length == 0
                    valor[:cant] = 0
                  else
                    @consulta1.each do |c|
                      if c['cantidad'] == ""
                        valor[:cant] = 0
                      else
                        valor[:cant] = c['cantidad'].to_i
                      end
                    end
                  end
                  graph1.push(valor)
                else
                  valor = {}
                  valor[:label] = st.Descripcion
                  valor[:cant] = 0
                  graph1.push(valor)
                end
              end

            else
              @users.each do |us|
                sql = "select u.name + ' ' + u.last_name as us_name, count(*) cantidad from clarifications c join users u on c.user_id = u.id where user_id = #{us.id} group by u.name, u.last_name"
                @consulta1 = ActiveRecord::Base::connection.exec_query(sql)

                if @consulta1.present? && !@consulta1.nil?
                  valor = {}
                  valor[:label] = us.name + " " + us.last_name
                  if @consulta1.length == 0
                    valor[:cant] = 0
                  else
                    @consulta1.each do |c|
                      if c['cantidad'] == ""
                        valor[:cant] = 0
                      else
                        valor[:cant] = c['cantidad'].to_i
                      end
                    end
                  end
                  graph1.push(valor)
                else
                  valor = {}
                  valor[:label] = us.name + " " + us.last_name
                  valor[:cant] = 0
                  graph1.push(valor)
                end
              end
            end


            @graph1 = graph1
          else
            @sin_info = true
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end
end
