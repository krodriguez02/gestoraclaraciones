class StatesController < ApplicationController
  before_action :set_state, only: [:show, :edit, :update, :destroy]

  @@byStates = "States"
  # GET /states
  # GET /states.json
  def index
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'States' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearStates = permisos.crear
          @editarStates = permisos.editar
          @leerStates = permisos.leer
          @eliminarStates = permisos.eliminar

          if permisos.view_name == @@byStates

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))

          #@states = State.all

          query = ""
          consulta = ""

          if params[:valorN].present?
            name = params[:valorN].to_s
            nombre = name.squeeze '%' #<==
            uN = nombre.last

            if uN == '%'
              nombre = nombre.chomp("%")
            end

            @nombre = ""
            prN = nombre[0]

            if prN == '%'
              var1 = nombre.gsub(/^./, "")
              @nombre = var1.gsub(/%/, " ")
            else
              @nombre = nombre.gsub(/%/, " ")
            end

            consulta = "Descripcion = '#{@nombre.to_s}'"
            query = "Descripción: " + @nombre.to_s
          end


          if params[:valorM].present?
            men = params[:valorM].to_s
            mensaje = men.squeeze '%' #<==
            uM = mensaje.last

            if uM == '%'
              mensaje = mensaje.chomp("%")
            end

            @mensaje = ""
            prM = mensaje[0]

            if prM == '%'
              var2 = mensaje.gsub(/^./, "")
              @mensaje = var2.gsub(/%/, " ")
            else
              @mensaje = mensaje.gsub(/%/, " ")
            end


            if consulta.bytesize > 0
              consulta += " and mensajeDefault = '#{@mensaje.to_s}'"
              query += " ,Mensaje Default: " + @mensaje.to_s
            else
              consulta = "mensajeDefault = '#{@mensaje.to_s}'"
              query = "Mensaje Default: " + @mensaje.to_s
            end

          end

          @states = State.where(consulta).page(params[:page]).per(10)

          if @states.present?
            @states = State.where(consulta).page(params[:page]).per(10)
            @query = query
          else
            @states = State.all.page(params[:page]).per(10)
            @error = true
          end


        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end


  # GET /states/1
  # GET /states/1.json
  def show
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'States' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearStates = permisos.crear
          @editarStates = permisos.editar
          @leerStates = permisos.leer
          @eliminarStates = permisos.eliminar

          if permisos.view_name == @@byStates

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if (@@leer == 2)


        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end

  end

  # GET /states/new
  def new
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'States' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarStates = permisos.editar

          if permisos.view_name == @@byStates

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8))
          @state = State.new
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /states/1/edit
  def edit
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'States' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarStates = permisos.editar

          if permisos.view_name == @@byStates

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@editar == 4))

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # POST /states
  # POST /states.json
  def create
    if current_user
      @state = State.new(state_params)



      respond_to do |format|
        if params[:yes].present?
          @state.falso_positivo = '1'
        elsif params[:no].present?
          @state.falso_positivo = '0'
        elsif params[:porDef].present?
          @state.falso_positivo = '2'
        end
        ul = State.last
        ultimo = ul.IdEstado
        @state.IdEstado =  ultimo + 1
        if @state.save
          format.html {redirect_to states_url, notice: 'State was successfully created.'}
          format.json {render :show, status: :created, location: @state}
        else
          format.html {render :new}
          format.json {render json: @state.errors, status: :unprocessable_entity}
        end
      end

    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # PATCH/PUT /states/1
  # PATCH/PUT /states/1.json
  def update
    if current_user
      respond_to do |format|
        if @state.update(state_params)
          format.html {redirect_to states_url, notice: 'State was successfully updated.'}
          format.json {render :show, status: :ok, location: @state}
        else
          format.html {render :edit}
          format.json {render json: @state.errors, status: :unprocessable_entity}
        end
      end

    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # DELETE /states/1
  # DELETE /states/1.json
  def destroy
    if current_user

      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'State' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarState = permisos.editar

          if permisos.view_name == @@byState

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@eliminar == 1 && @state.id != 1))
          @state.destroy
          respond_to do |format|
            format.html {redirect_to states_path, notice: 'State was successfully destroyed.'}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end


  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_state
    @state = State.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def state_params
    params.require(:state).permit(:IdEstado, :Descripcion, :falso_positivo, :mensajeDefault)
  end
end
