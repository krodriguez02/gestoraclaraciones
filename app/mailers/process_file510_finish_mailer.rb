class ProcessFile510FinishMailer < ApplicationMailer

  default :from => "krodriguez@kssoluciones.com.mx"
  # default :from => "kmprevencion@liverpool.com.mx"

  def finished(file, user, total, processed, no_update, no_find, no_hist, inicio, fin)
    @file = file
    @user = user
    @total = total
    @processed = processed
    @no_up = no_update
    @no_find = no_find
    @no_hist = no_hist
    @inicio = inicio
    @fin = fin

    mail(:to => "#{@user.email}", :subject => 'PROSA510 file processing finished')
  end

  def terminado(file, user, total, processed, no_update, no_find, no_hist, inicio, fin)
    @file = file
    @user = user
    @total = total
    @processed = processed
    @no_up = no_update
    @no_find = no_find
    @no_hist = no_hist
    @inicio = inicio
    @fin = fin

    mail(:to => "#{@user.email}", :subject => 'Procesamiento de archivo PROSA510 terminado')
  end
end
