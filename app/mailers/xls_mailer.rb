class XlsMailer < ApplicationMailer

  default :from => "krodriguez@kssoluciones.com.mx"
  # default :from => "kmprevencion@liverpool.com.mx"

  def enviar_xls(fecha_comienzo, usuario, file, fecha_termino)
    @fecha_comienzo = fecha_comienzo
    @usuario = usuario
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/'+ @file
    @fin = fecha_termino

    attachments[@file] = File.read("#{@path_file}")
    mail(:to => "#{@usuario.email}", :subject => 'Exportación a XLS de filtro de aclaraciones')
  end

  def send_xls(fecha_comienzo, usuario, file, fecha_termino)
    @fecha_comienzo = fecha_comienzo
    @usuario = usuario
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/'+ @file
    @fin = fecha_termino

    attachments[@file] = File.read("#{@path_file}")
    mail(:to => "#{@usuario.email}", :subject => 'XLS Export Clarifications Filter')
  end
end
