class CargaProsa < ApplicationRecord
  has_many :users
  has_attached_file :name, :default_url => "/:basename.:extension"

  validates_attachment :name, presence: true,
                       content_type: {content_type: ["text/plain",
                                                     "application/pdf",
                       ]
                       },
                       message: ' Only PDF and text plain files are allowed.'

end
