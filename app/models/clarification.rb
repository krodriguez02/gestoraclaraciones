class Clarification < ApplicationRecord
  has_many :transaction_clarifications, foreign_key: :idClarification
  belongs_to :user, foreign_key: :user_id
  belongs_to :state, foreign_key: :state_id, primary_key: :IdEstado
  has_many :clarification_type, foreign_key: :clarification_type_id


  # has_attached_file :name, :default_url => "/:basename.:extension"
  #
  # validates_attachment :name, presence: true,
  #                      content_type: {content_type: ["application/pdf",
  #                      ]
  #                      },
  #                      message: ' Only PDF and text plain files are allowed.'
  #

end
