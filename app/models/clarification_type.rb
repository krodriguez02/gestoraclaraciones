class ClarificationType < ApplicationRecord
  has_many :clarifications, foreign_key: :clarification_type_id, primary_key: :id
end

