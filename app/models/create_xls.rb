class CreateXls
  def setInfo(user, clarifications)
    @user = user
    @clarifications = clarifications
  end

  def clarificationsXlsEs
    begin
      puts '>------------------------------------------------------------------------<'
      puts green('Iniciando Hilo: Creación de XLS-Aclaraciones')
      puts green('Cantidad de registros: ' + @clarifications.count().to_s)
      @fecha_comienzo = Time.now
      ## -------------------- COMIENZA LA CREACIÓN DEL EXCEL ------------------------##

      ##------------------------------ ANCHO DE LAS FILAS ------------------------------- ##
      col_widths = [10, 25, 20, 20, 30, 20, 30, 20, 10]
      p = Axlsx::Package.new
      p.use_autowidth = true
      wb = p.workbook

      sleep(2)

      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "B7328C", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "685963", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        main_title_cell = style.add_style(bg_color: "B7328C", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)

        ##**************************----------------------- HOJA ALERTAS -------------------------**************************##
        wb.add_worksheet(name: "Aclaraciones") do |sheet|

          #FILAS COMBINADAS
          sheet.merge_cells('C2:H5') #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells('E6:H6') #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoLiverpoolInicio.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 101
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          sheet.add_row [" ", " ", "Aclaraciones", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]


          ##----------------------- ALERTAS -------------------------#
          sheet.add_row [" ", "Folio", "Fecha/Hora", "Numero de Tarjeta", "Nombre del cliente", "Usuario", "Tipo de Aclaracion", "Estado", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]

          @clarifications.each do |al|
            @folio = al.folio
            @fecha_hora = al.created_at.strftime('%Y-%m-%d %H:%M')
            @card_number = al.card_number
            if al.client_name.nil?
              @nombre = ""
            else
              @nombre = al.client_name
            end
            if al.last_name_f.nil?
              @ap_f = ""
            else
              @ap_f = al.last_name_f
            end
            if al.last_name_m.nil?
              @ap_m = ""
            else
              @ap_m = al.last_name_m
            end

            @cliente = @nombre + " " + @ap_f + " " + @ap_m
            if al.user.present?
              @usuario = al.user.name + ' ' + al.user.last_name
            else
              @usuario = 'N/A'
            end
            @type = ClarificationType.find(al.clarification_type_id)
            if @type.present? && !@type.nil?
              @type = @type.name
            else
              @type = "N/A"
            end
            if al.state.present?
              @state = al.state.Descripcion
            else
              @state = "N/A"
            end
            sheet.add_row [" ", @folio, @fecha_hora, @card_number, @cliente, @usuario, @type, @state, " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          ##----------------------- CERRANDO HOJA 1 -------------------------##
          sheet.column_widths *col_widths

          puts('Terminando hoja 1...')
          sleep(2)
        end

      end


      ##-------------------- GUARDANDO EXCEL ----------------------##
      @file = "Clarifications-#{Time.now.strftime('%H%M%S')}.xls"
      p.serialize("public/generatedXLS/#{@file}")


      puts 'Archivo creado: ' + @file

      puts('Archivo almacenado...')

      @fecha_termino = Time.now
      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #
      usuario = User.find(@user)
      XlsMailer.enviar_xls(@fecha_comienzo, usuario, @file, @fecha_termino).deliver

      puts 'Listo, proceso terminado'
    rescue => e
      puts red('Archivo: log/error_xls.log')
      logger = Logger.new("log/error_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
  end

  def clarificationsXlsEn
    begin
      puts '>------------------------------------------------------------------------<'
      puts green('Iniciando Hilo: Creación de XLS-Aclaraciones')
      puts green('Cantidad de registros: ' + @clarifications.count().to_s)
      @fecha_comienzo = Time.now
      ## -------------------- COMIENZA LA CREACIÓN DEL EXCEL ------------------------##

      ##------------------------------ ANCHO DE LAS FILAS ------------------------------- ##
      col_widths = [10, 20, 20, 20, 20, 20, 20, 20, 10]
      p = Axlsx::Package.new
      p.use_autowidth = true
      wb = p.workbook

      sleep(2)

      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "B7328C", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "685963", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        main_title_cell = style.add_style(bg_color: "B7328C", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)

        ##**************************----------------------- HOJA ALERTAS -------------------------**************************##
        wb.add_worksheet(name: "Clarifications") do |sheet|

          #FILAS COMBINADAS
          sheet.merge_cells('C2:H5') #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells('E6:H6') #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoLiverpoolInicio.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 101
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          sheet.add_row [" ", " ", "CLarifications", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]


          ##----------------------- ALERTAS -------------------------#
          sheet.add_row [" ", "Folio", "Date/Hour", "Card Number", "Clients Name", "User", "Clarification Type", "State", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]

          @clarifications.each do |al|
            @folio = al.folio
            @fecha_hora = al.created_at.strftime('%Y-%m-%d %H:%M')
            @card_number = al.card_number
            @cliente = al.client_name + " " + al.last_name_f + al.last_name_m
            if al.user.present?
              @usuario = al.user.name + ' ' + al.user.last_name
            else
              @usuario = 'N/A'
            end
            @type = ClarificationType.find(al.clarification_type_id)
            if @type.present? && !@type.nil?
              @type = @type.name
            else
              @type = "N/A"
            end
            if al.state.present?
              @state = al.state.Descripcion
            else
              @state = "N/A"
            end
            sheet.add_row [" ", @folio, @fecha_hora, @card_number, @cliente, @usuario, @type, @state, " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          ##----------------------- CERRANDO HOJA 1 -------------------------##
          sheet.column_widths *col_widths

          puts('Terminando hoja 1...')
          sleep(2)
        end

      end


      ##-------------------- GUARDANDO EXCEL ----------------------##
      p.serialize("public/generatedXLS/Clarifications-#{Time.now.strftime('%H%M%S')}.xlsx")
      @file = "Clarifications-#{Time.now.strftime('%H%M%S')}.xls"

      puts 'Archivo creado: ' + @file

      puts('Archivo almacenado...')

      @fecha_termino = Time.now
      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #
      usuario = User.find(@user)
      XlsMailer.send_xls(@fecha_comienzo, usuario, @file, @fecha_termino).deliver

      puts 'Listo, proceso terminado'
    rescue => e
      puts red('Archivo: log/error_xls.log')
      logger = Logger.new("log/error_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end
end