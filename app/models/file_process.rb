class FileProcess

  def setInfo(idioma, file, filen, user, idiom, tts)
    @idioma = idioma
    @file = file
    @filen = filen
    @user = user
    @idioma = idiom
    @tts = tts

    @true = true
  end

  def processPROSA510
    begin
      puts '>------------------------------------------------------------------------<'
      puts green('Iniciando Hilo: Procesamiento archivo PROSA510: ' + @filen)
      @fecha_comienzo = Time.now.to_datetime.strftime('%Y-%m-%d %H:%M')
      puts green('Hora comienzo: ' + @fecha_comienzo)
      @lineas_noprocess_up = 0
      @lineas_noprocess_find = 0
      @ln_process = 0
      @ln_total = 0
      @ln_no_hist = 0

      logger = Logger.new("log/prosa510_process_tran.log")
      logger.error('---------------------------------------------------------- ARCHIVO PROSA510: ' + @filen)

      File.open(@file).each do |ln|
        line = ln.gsub(/\s+/, "")

        if line != ""
          if (!line.include? "page") && (!line.include? "HEADER") && (!line.include? "TRAILER")
            @ln_total+= 1
            #puts 'Lineas totales: '+@ln_total.to_s
            numero_cta = ln[5..23]
            fecha_consumo = ln[28..33]
            numero_autorizacion = ln[385..390]
            referencia = ln[396..418]


            tt = 'TT20' + fecha_consumo + '0101'

            if @tts.include? tt
                # select = "Select Numero_de_Tarjeta, Codigo_Aprov from KMNADM_DEV.dbo.#{tt} where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                select = "Select Numero_de_Tarjeta, Codigo_Aprov from kmgestorLiverpoolv4.dbo.#{tt} where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                Timeout::timeout(0) {@tran = ActiveRecord::Base::connection.exec_query(select)}

                if @tran.present? && !@tran.nil?
                  @tran.each do |tr|
                    @id_tran = tr['Codigo_Aprov']

                    # update = "Update KMNADM_DEV.dbo.#{tt} set referencia23 = '#{referencia}' where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                    update = "Update kmgestorLiverpoolv4.dbo.#{tt} set referencia23 = '#{referencia}' where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                    Timeout::timeout(0) {@tran_up = ActiveRecord::Base::connection.exec_query(update)}

                    if !@tran_up.present? && @tran_up.nil?
                      @lineas_noprocess_up+= 1
                      #puts 'Linea no actualizada: ' + @lineas_noprocess_up.to_s
                      logger.error('Transacción no actualizada: ' + ln)
                    else
                      @ln_process+= 1
                      #puts 'Linea procesada: ' + @ln_process.to_s
                    end
                  end
                else
                  fecha_ant = '20' + fecha_consumo
                  fecha_ant = fecha_ant.to_date
                  fecha_ant = fecha_ant - 1.day
                  fecha_ant = fecha_ant.strftime('%Y%m%d')

                  #----------------------------- REVISO UN DIA ANTERIOR A LA FECHA DEL ARCHIVO PROSA
                  tt = 'TT' + fecha_ant + '0101'

                  if @tts.include? tt
                    # select = "Select Numero_de_Tarjeta, Codigo_Aprov from KMNADM_DEV.dbo.#{tt} where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                    select = "Select Numero_de_Tarjeta, Codigo_Aprov from kmgestorLiverpoolv4.dbo.#{tt} where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                    Timeout::timeout(0) {@tran = ActiveRecord::Base::connection.exec_query(select)}

                    if @tran.present? && !@tran.nil?
                      @tran.each do |tr|
                        @id_tran = tr['Codigo_Aprov']

                        # update = "Update  KMNADM_DEV.dbo.#{tt} set referencia23 = '#{referencia}' where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                        update = "Update kmgestorLiverpoolv4.dbo.#{tt} set referencia23 = '#{referencia}' where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                        Timeout::timeout(0) {@tran_up = ActiveRecord::Base::connection.exec_query(update)}

                        if !@tran_up.present? && @tran_up.nil?
                          @lineas_noprocess_up+= 1
                          #puts 'Linea no actualizada: ' + @lineas_noprocess_up.to_s
                          logger.error('Transacción no actualizada: ' + ln)
                        else
                          @ln_process+= 1
                          #puts 'Linea procesada: ' + @ln_process.to_s
                        end
                      end
                    else
                      fecha_ant = '20' + fecha_consumo
                      fecha_ant = fecha_ant.to_date
                      fecha_ant = fecha_ant + 1.day
                      fecha_ant = fecha_ant.strftime('%Y%m%d')

                      #----------------------------- REVISO UN DIA POSTERIOR A LA FECHA DEL ARCHIVO PROSA
                      tt = 'TT' + fecha_ant + '0101'

                      if @tts.include? tt
                        # select = "Select Numero_de_Tarjeta, Codigo_Aprov from KMNADM_DEV.dbo.#{tt} where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                        select = "Select Numero_de_Tarjeta, Codigo_Aprov from kmgestorLiverpoolv4.dbo.#{tt} where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                        Timeout::timeout(0) {@tran = ActiveRecord::Base::connection.exec_query(select)}

                        if @tran.present? && !@tran.nil?
                          @tran.each do |tr|
                            @id_tran = tr['Codigo_Aprov']

                            # update = "Update KMNADM_DEV.dbo.#{tt} set referencia23 = '#{referencia}' where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                            update = "Update kmgestorLiverpoolv4.dbo.#{tt} set referencia23 = '#{referencia}' where Numero_de_Tarjeta like '#{numero_cta}%' and Fecha_Trans = '#{fecha_consumo}' and Codigo_Aprov like '#{numero_autorizacion}%'"
                            Timeout::timeout(0) {@tran_up = ActiveRecord::Base::connection.exec_query(update)}

                            if !@tran_up.present? && @tran_up.nil?
                              @lineas_noprocess_up+= 1
                              #puts 'Linea no actualizada: ' + @lineas_noprocess_up.to_s
                              logger.error('Transacción no actualizada: ' + ln)
                            else
                              @ln_process+= 1
                              #puts 'Linea procesada: ' + @ln_process.to_s
                            end
                          end
                        else
                          #---------------------------- DEFINITIVAMENTE NO SE ENCONTRÓ
                          @lineas_noprocess_find+= 1
                          #puts 'Linea no encontrada: ' + @lineas_noprocess_find.to_s
                          logger.error('Transacción no encontrada: ' + ln)
                        end
                      else
                        @ln_no_hist+= 1
                        puts red('ERROR: Sin tabla transaccional: ' + tt)
                      end
                    end
                  else
                    @ln_no_hist+= 1
                    puts red('ERROR: Sin tabla transaccional: ' + tt)
                  end
                end
            else
              @ln_no_hist+= 1
              puts red('ERROR: Sin tabla transaccional: ' + tt)
            end
          end
        end
      end

      puts 'Lineas totales: '+@ln_total.to_s
      puts 'Linea procesada: ' + @ln_process.to_s
      puts 'Linea no actualizada: ' + @lineas_noprocess_up.to_s
      puts 'Linea no encontrada: ' + @lineas_noprocess_find.to_s
      puts 'Linea sin historial: ' + @ln_no_hist.to_s


      # Envio de correos.
      @fin = Time.now.to_datetime.strftime('%Y-%m-%d %H:%M')
      if @idioma == "en"
        ProcessFile510FinishMailer.finished(@filen, @user, @ln_total.to_s, @ln_process.to_s, @lineas_noprocess_up.to_s, @lineas_noprocess_find.to_s, @ln_no_hist.to_s, @fecha_comienzo, @fin).deliver
      elsif @idioma == "es"
        ProcessFile510FinishMailer.terminado(@filen, @user, @ln_total.to_s, @ln_process.to_s, @lineas_noprocess_up.to_s, @lineas_noprocess_find.to_s, @ln_no_hist.to_s, @fecha_comienzo, @fin).deliver
      end


    rescue => e
      puts red('Archivo: log/prosa510_process.log')
      logger = Logger.new("log/prosa510_process.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end
end
