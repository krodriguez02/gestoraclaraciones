class FilesClarification < ApplicationRecord
  belongs_to :clarification, foreign_key: :idClarification
  belongs_to :user, foreign_key: :user_id

  has_attached_file :name, :default_url => "/:basename.:extension"

  validates_attachment :name, presence: true,
                       content_type: {content_type: ["application/pdf",
                       ]
                       },
                       message: ' Only PDF and text plain files are allowed.'
end
