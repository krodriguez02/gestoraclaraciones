class State < ApplicationRecord
  has_many :clarifications, foreign_key: :IdEstado

  self.primary_key = :IdEstado
end
