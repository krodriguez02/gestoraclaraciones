class TransactionClarification < ApplicationRecord
  belongs_to :clarification, foreign_key: :idClarification
  belongs_to :user, foreign_key: :userMarked
end
