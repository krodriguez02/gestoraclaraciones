class ClarifPdf < Prawn::Document

  Prawn::Font::AFM.hide_m17n_warning = true

  def initialize(clarification, loc)
    super()
    @clarification = clarification
    @loc = loc
    puts @loc
    @clarification.each do |clari|
      name = clari.client_name
      lastf = clari.last_name_f
      lastm = clari.last_name_m
      @name = name + " " + lastf + " " + lastm
      @card = clari.card_number
      @phone = clari.phone
      @email = clari.email
      @accty = clari.account_type
      @fol = clari.folio
      @date = clari.payment_date
      @bank = clari.bank
      @amo = clari.amount
      @created = clari.created_at
      @clartype = clari.internal_clarification_type
      @auto = clari.key_autorization
      @atm = clari.atm_type
      @facts = clari.facts
      @idC = clari.id

    end

    @trans = TransactionClarification.where('idClarification = ?', @idC)

    hoja1
    hoja2
    line_items
    text_content

  end

  def hoja1
    a1 = cursor - -30 #hoja 1
    image "#{Rails.root}/app/pdfs/images/liverpoolicono.png", :at => [-30, a1], width: 105, height: 80 #hoja1
    bounding_box([230, a1], width: 700, height: 50) do
      font_size 25
      move_down 20
      text "<b>Liverpool</b>", :inline_format => true
    end

    b1 = cursor - 20
    bounding_box([150, b1], width: 700, height: 50) do
      font_size 25
      if @loc == "es"
        text "<b>Solicitud de Aclaración</b>", :inline_format => true
      elsif @loc == "en"
        text "<b>Clarification Request</b>", :inline_format => true
      end

    end

    b2 = cursor - 10
    bounding_box([-15, b2], width: 700, height: 50) do
      font_size 10
      text "<b>Folio: </b>", :inline_format => true
      move_down 5
      text "#{@fol}" #Folio
    end
    bounding_box([400, b2], width: 700, height: 50) do
      font_size 10
      if @loc == "es"
        text "<b>Fecha: </b>", :inline_format => true
      elsif @loc == "en"
        text "<b>Date: </b>", :inline_format => true
      end

      move_down 5
      text "#{@created}" #Fecha hora de aclaración
    end

    c1 = cursor - 20
    bounding_box([-15, c1], width: 700, height: 50) do
      font_size 15
      if @loc == "es"
        text "<b>Datos Generales</b>", :inline_format => true #datos generales
      elsif @loc == "en"
        text "<b>General Data</b>", :inline_format => true #datos generales
      end

      stroke_color "610443"
      stroke do
        horizontal_line 0, 570, :at => 20 #linea datos generales
      end
    end

    d1 = cursor - 10
    bounding_box([-15, d1], width: 700, height: 50) do
      font_size 10
      if @loc == "es"
        text "<b>Nombre del Tarjetahabiente</b>", :inline_format => true #Nombre del Tarjetahabiente en datos generales
      elsif @loc == "en"
        text "<b>Cardholder's Name</b>", :inline_format => true #Nombre del Tarjetahabiente en datos generales
      end
      move_down 5
      text "#{@name}" #Nombre del Tarjetahabiente
    end
    bounding_box([200, d1], width: 700, height: 50) do
      font_size 10
      if @loc == "es"
        text "<b>Número de Tarjeta</b>", :inline_format => true #Tarjeta en datos generales
      elsif @loc == "en"
        text "<b>Card Number</b>", :inline_format => true #Tarjeta en datos generales
      end
      move_down 5
      text "#{@card}" #Tarjeta
    end
    bounding_box([400, d1], width: 700, height: 50) do
      font_size 10
      if @loc == "es"
        text "<b>Correo Electrónico</b>", :inline_format => true #correo en datos generales
      elsif @loc == "en"
        text "<b>Email</b>", :inline_format => true #correo en datos generales
      end
      move_down 5
      text "#{@email}" #correo
    end

    e1 = cursor - 5
    bounding_box([-15, e1], width: 700, height: 50) do
      font_size 10
      if @loc == "es"
        text "<b>Teléfono de Casa</b>", :inline_format => true #teléfono en datos generales
      elsif @loc == "en"
        text "<b>Home Phone</b>", :inline_format => true #teléfono en datos generales
      end

      move_down 5
      text "#{@phone}" #teléfono
    end


    f1 = cursor - -75
    bounding_box([350, f1], width: 50, height: 50) do
      y = 0
      stroke do
        rectangle [-150, y + 25], 10, 10
      end
    end
    g1 = cursor - -23
    p2 = cursor - -25

    if @accty == 1
      image "#{Rails.root}/app/pdfs/images/paloma.png", :at => [200, p2], width: 10, height: 10 #paloma
    end
    bounding_box([215, g1], width: 55, height: 50) do
      if @loc == "es"
        text "<b>Titular</b>", :inline_format => true #titular en datos generales
      elsif @loc == "en"
        text "<b>Cardholder</b>", :inline_format => true #titular en datos generales
      end

    end

    if @accty == 2
      image "#{Rails.root}/app/pdfs/images/paloma.png", :at => [400, p2], width: 10, height: 10 #paloma
    end

    bounding_box([600, f1], width: 50, height: 50) do
      y = 0
      stroke do
        rectangle [-200, y + 25], 10, 10
      end
    end

    bounding_box([417, g1], width: 50, height: 50) do
      if @loc == "es"
        text "<b>Adicional</b>", :inline_format => true #adicional en datos generales
      elsif @loc == "en"
        text "<b>Additional Card</b>", :inline_format => true #adicional en datos generales
      end

    end

    #-------------------- TIPO DE ACLARACIÓN --------------------#

    h1 = cursor - 20

    bounding_box([-15, h1], width: 700, height: 50) do
      font_size 15
      if @loc == "es"
        text "<b>Tipo de Aclaración</b>", :inline_format => true #datos generales
      elsif @loc == "en"
        text "<b>Clarification Type</b>", :inline_format => true #datos generales
      end

      stroke_color "610443"
      stroke do
        horizontal_line 0, 570, :at => 20 #linea datos generales
      end
    end
    #-------------------- TIPO DE ACLARACIÓN --------------------#

    j1 = cursor - 5
    bounding_box([-15, j1], width: 510, height: 20) do #Tipo de aclaracion
      font_size 10
      if @clartype == 1
        if @loc == "es"
          text "Pago no Aplicado", :align => :center, :inline_format => true
        elsif @loc == "en"
          text "Payment not applied", :align => :center, :inline_format => true
        end
      elsif @clartype == 2
        if @loc == "es"
          text "Disposición en cajero automático", :inline_format => true
        elsif @loc == "en"
          text "ATM Layout", :inline_format => true
        end
      elsif @clartype == 3
        if @loc == "es"
          text "Compra en comercio", :inline_format => true
        elsif @loc == "en"
          text "Purchase in commerce", :inline_format => true
        end

      elsif @clartype == 4
        if @loc == "es"
          text "Devolución", :inline_format => true
        elsif @loc == "en"
          text "Refund", :inline_format => true
        end

      end
    end
    #-------------------- TIPO DE ACLARACIÓN(va a depender de la opcion marcada) --------------------#
    k1 = cursor - 60
    bounding_box([-15, k1], width: 700, height: 50) do
      font_size 15

      if @clartype == 1
        if @loc == "es"
          text "<b>Pago no Aplicado</b>", :inline_format => true
        elsif @loc == "en"
          text "<b>Payment not applied</b>", :inline_format => true
        end
      elsif @clartype == 2
        if @loc == "es"
          text "<b>Disposición en cajero automático</b>", :inline_format => true
        elsif @loc == "en"
          text "<b>Disposition at an ATM</b>", :inline_format => true
        end
      elsif @clartype == 3
        if @loc == "es"
          text "Compra en comercio", :inline_format => true
        elsif @loc == "en"
          text "Purchase in commerce", :inline_format => true
        end
      elsif @clartype == 4
        if @loc == "es"
          text "Devolución", :inline_format => true
        elsif @loc == "en"
          text "Refund", :inline_format => true
        end
      end
      stroke_color "610443"
      stroke do
        horizontal_line 0, 570, :at => 20 #linea tipo de aclaracion
      end
    end
    #-------------------- TIPO DE ACLARACIÓN(va a depender de la opcion marcada) --------------------#


    if @clartype == 1
#-------------------- PAGO NO APLICADO --------------------#
      l1 = cursor - 5
      bounding_box([-15, l1], width: 200, height: 50) do
        font_size 10
        if @loc == "es"
          text "<b>Fecha de Pago</b>", :inline_format => true
        elsif @loc == "en"
          text "<b>Payment Date</b>", :inline_format => true
        end
        move_down 5
        text "#{@date}"
      end #BOUNDING BOX
      bounding_box([200, l1], width: 200, height: 50) do
        font_size 10
        if @loc == "es"
          text "<b>Importe de Pago</b>", :inline_format => true
        elsif @loc == "en"
          text "<b>Payment Amount</b>", :inline_format => true
        end
        move_down 5
        #text "#{ActiveSupport::NumberHelper::number_to_currency(@total_pts, {precision: 2, unit: ''})}"
        text "$#{@amo}"
        #text "$#{ActiveSupport::NumberHelper::number_to_currency(@amo, {precision: 2, unit: '',separator: "."})}"
      end
      bounding_box([400, l1], width: 200, height: 50) do
        font_size 10
        if @loc == "es"
          text "<b>Clave Número de Autorización</b>", :inline_format => true
        elsif @loc == "en"
          text "<b>Key Authorization Number</b>", :inline_format => true
        end
        move_down 5
        text "#{@auto}"
      end

      m1 = 200
      bounding_box([-15, m1], width: 200, height: 50) do
        font_size 10
        if @loc == "es"
          text "<b>Banco o Establecimiento</b>", :inline_format => true
        elsif @loc == "en"
          text "<b>Bank or Establishment</b>", :inline_format => true
        end
        move_down 5
        text "#{@bank}"
      end

#-------------------- PAGO NO APLICADO --------------------#

    elsif @clartype == 2
#-------------------- DISPOSICION EN CAJERO --------------------#
      move_down 20
      font_size 10
      p1 = cursor - -5
      f1 = cursor - -30

      if @atm == '1'
        image "#{Rails.root}/app/pdfs/images/paloma.png", :at => [0, p1], width: 10, height: 10 #paloma
      end
      bounding_box([150, f1], width: 50, height: 50) do
        y = 0
        stroke do
          rectangle [-150, y + 25], 10, 10
        end
      end
      g1 = cursor - -23
      bounding_box([20, g1], width: 205, height: 50) do
        if @loc == "es"
          text "<b>El cajero entregó parcialmente la cantidad solicitada</b>", :inline_format => true #titular en datos generales
        elsif @loc == "en"
          text "<b>The cashier partially delivered the requested amount</b>", :inline_format => true #titular en datos generales
        end
      end

      if @atm == '2'
        image "#{Rails.root}/app/pdfs/images/paloma.png", :at => [250, p1], width: 10, height: 10 #paloma
      end
      bounding_box([400, f1], width: 50, height: 50) do
        y = 0
        stroke do
          rectangle [-150, y + 25], 10, 10
        end
      end
      g1 = cursor - -23
      bounding_box([265, g1], width: 150, height: 50) do
        if @loc == "es"
          text "<b>El cajero no entregó efectivo</b>", :inline_format => true #titular en datos generales
        elsif @loc == "en"
          text "<b>The cashier did not deliver cash</b>", :inline_format => true #titular en datos generales
        end
      end

      if @atm == '3'
        image "#{Rails.root}/app/pdfs/images/paloma.png", :at => [430, p1], width: 10, height: 10 #paloma
      end
      bounding_box([580, f1], width: 50, height: 50) do
        y = 0
        stroke do
          rectangle [-150, y + 25], 10, 10
        end
      end
      g1 = cursor - -23
      bounding_box([445, g1], width: 150, height: 50) do
        if @loc == "es"
          text "<b>Disposición no reconocida</b>", :inline_format => true #titular en datos generales
        elsif @loc == "en"
          text "<b>Disposal of cash not recognized</b>", :inline_format => true #titular en datos generales
        end
      end

#-------------------- DISPOSICION EN CAJERO --------------------#

#-------------------- COMPRA EN COMERCIO --------------------#
    elsif @clartype == 3
      h1 = cursor - -5
      bounding_box([-15, h1], width: 600, height: 50) do
        font_size 11
        text "#{@facts.to_s}", :inline_format => true
      end
#-------------------- COMPRA EN COMERCIO --------------------#

    end #CLARTYPE ==1


  end

  def hoja2
    start_new_page
    a2 = cursor - -30 #hoja 1
    image "#{Rails.root}/app/pdfs/images/liverpoolicono.png", :at => [-30, a2], width: 105, height: 80 #hoja1
    bounding_box([230, a2], width: 700, height: 50) do
      font_size 25
      move_down 20
      text "<b>Liverpool</b>", :inline_format => true
    end

    b2 = cursor - 10
    bounding_box([150, b2], width: 700, height: 50) do
      font_size 25
      if @loc == "es"
        text "<b>Solicitud de Aclaración</b>", :inline_format => true
      elsif @loc == "en"
        text "<b>Clarification Request</b>", :inline_format => true
      end
    end


#-------------------- AQUI VA LA TABLA --------------------#
  end

  def line_items
    move_down 20
    table line_item_rows, :cell_style => {size: 8} do
      #font_size 8
      row(0).font_style = :bold
      row(0).size = 10
      row(0).background_color = "B32787"
      columns(0..5).align = :center
      self.row_colors = ["DDDDDD", "FFFFFF"]
      self.header = true
    end
  end

  def line_item_rows
    if @loc == "es"
      @folio = "Folio de Aclaración"
      @fecha = "Fecha"
      @ref = "Referencia a 23 Posiciones"
      @com = "Comercio"
      @i1 = "Importe 1"
      @i2 = "Importe 2"
    elsif @loc == "en"
      @folio = "Clarification Sheet"
      @fecha = "Date"
      @ref = "Reference to 23 Positions"
      @com = "Commerce"
      @i1 = "Amount 1"
      @i2 = "Amount 2"
    end

    [[@folio, @fecha, @ref, @com, @i1, @i2]] +
        #@clarification.map do |item|
        @trans.map do |item|
          idC = Clarification.find(item.idClarification)
          idClar = idC.folio
          comercio = item.comercio

          if comercio == ""
            @comercio = "N/A"
          else
            @comercio = item.comercio
          end

          if item.importe1.present?
            #@v = number_to_currency(item.importe1.to_f / 100, locale: :en)
            #@imp = Float(@v.to_f / 100.00)
            @imp = ActiveSupport::NumberHelper::number_to_currency(item.importe1.to_f / 100, locale: :en)
            puts @imp
          else
            @imp = 0.00
          end

          if item.importe2.present?
            # @v2 = item.importe2.to_i
            # @imp2 = Float(@v2.to_f / 100.00)
            @imp2 = ActiveSupport::NumberHelper::number_to_currency(item.importe2.to_f / 100, locale: :en)
            puts @imp2
          else
            @imp2 = 0.00
          end


          [idClar, item.fecha_hora, item.referencia23, @comercio, @imp, @imp2]
          #[idClar, item.fecha_hora, item.referencia23, @comercio, item.importe1, item.importe2]
        end

  end

  def text_content
    b3 = cursor - 90
    if @loc == "es"
      @text = "En virtud de lo anterior solicito de Liverpool PC, S.A. de C.V.,me dé respuesta de las transacciones descritas en el
presente escrito de Aclaración.Declaro que la información aquí mencionada es completa y exacta. Asumo total
responsabilidad de la veracidad de la misma y acepto que la compañía tendrá derecho de exigir toda clase de información
sobre los hechos relacionados con el acontecimiento y por los cuales puedan determinarse las circunstancias de su
realización y las consecuencias del mismo, incluso información adicional a la que en principio sea solicitada y entregada. Sí
como resultado de la investigación que se llevará a cabo se establece que para estos cargos se cumplió con los
lineamientos de operación, me obligo a pagar el importe total de los cargos reclamados así como intereses ordinarios,
comisiones, impuestos y demás conceptos accesorios a partir de la fecha e compra; así también la cantidad de $180.00
pesos más I.V.A misma que será cargada a mi cuenta por el servicio de aclaración aquí solicitado."
    elsif @loc == "en"
      @text = "In virtue of the above I request from Liverpool PC, S.A. of C.V., to give me an answer of the transactions described in the
present written Clarification. I declare that the information mentioned here is complete and accurate. I assume total
responsibility of the veracity of it and I accept that the company will have the right to demand all kinds of information
about the facts related to the event and by which the circumstances of its
realization and the consequences of it, including additional information that is requested and delivered in principle. Yes
as a result of the investigation that will be carried out, it is established that for these charges the
operating guidelines, I am obliged to pay the total amount of the charges claimed as well as ordinary interests,
commissions, taxes and other accessory items from the date of purchase; so also the amount of $ 180.00
pesos plus I.V.A same that will be charged to my account by the clarification service requested here."
    end

    bounding_box([-15, b3], width: 550, height: 300) do
      font_size 10
      text "#{@text}", :align => :center
    end

    h3 = cursor - 20
    bounding_box([230, h3], width: 700, height: 50) do
      font_size 10
      text "<b>Atentamente</b>", :inline_format => true #tipo de aclaracion datos generales
    end
    j3 = cursor - 20
    bounding_box([230, j3], width: 700, height: 50) do
      stroke_color "000000"
      stroke do
        horizontal_line -50, 120, :at => 20 #linea tipo de aclaracion
      end
    end
    i3 = cursor - 0
    bounding_box([225, i3], width: 700, height: 50) do
      font_size 10
      if @loc == "es"
        text "<b>Nombre y Firma</b>", :inline_format => true #tipo de aclaracion datos generales
      elsif @loc == "en"
        text "<b>Name and signature</b>", :inline_format => true #tipo de aclaracion datos generales
      end

    end

  end
end