class ClarificationPdf < Prawn::Document

  Prawn::Font::AFM.hide_m17n_warning = true

  def initialize(clarification)
    super()
    @clarification = clarification

    @clarification.each do |clari|
      name = clari.client_name
      lastf = clari.last_name_f
      lastm = clari.last_name_m
      @name = name + " " + lastf + " " + lastm
      @card = clari.card_number
      @phone = clari.phone
      @email = clari.email
      @accty = clari.account_type
      @fol = clari.folio
      @date = clari.payment_date
      @bank = clari.bank
      @amo = clari.amount
      @created = clari.created_at
    end


    hoja1
    hoja2
    #line_items

  end


  def hoja1


    a1 = cursor - -36 #hoja 1
    image "#{Rails.root}/app/pdfs/images/Hoja1.jpg", :at => [-120, a1], width: 760, height: 790 #hoja1

    b1 = cursor - 104
    bounding_box([350, b1], width: 600, height: 20) do
      text "#{Time.now.to_date}" #fecha de solicitud
    end

    c1 = cursor - 14 #nombre del tarjetahabiente
    bounding_box([30, c1], width: 200, height: 20) do
      text "#{@name.to_s}" #nombre del tarjetahabiente
    end
    bounding_box([260, c1], width: 200, height: 20) do
      text "#{@card.to_s}" #nombre del tarjetahabiente
    end

    d1 = cursor - 6
    bounding_box([30, d1], width: 100, height: 20) do
      font_size 11
      text "#{@phone.to_s}" #nombre del tarjetahabiente
    end
    bounding_box([160, d1], width: 200, height: 20) do
      font_size 11
      text "#{@email.to_s}" #correo electronico
    end


  end

  def hoja2
    start_new_page

    a2 = cursor - -36 #hoja 1

    image "#{Rails.root}/app/pdfs/images/Hoja2.jpg", :at => [-120, a2], width: 760, height: 790 #hoja2

    b2 = cursor - 80
    bounding_box([400, b2], width: 100, height: 10) do
      font_size 10
      text "#{@created.to_date}" #folio tabla
    end


    c2 = cursor - 90
    bounding_box([-10, c2], width: 100, height: 20) do
      font_size 7
      text "#{@fol.to_s}" #folio tabla
    end
    bounding_box([95, c2], width: 100, height: 20) do
      font_size 7
      text "#{@date.to_s}" #fecha tabla
    end
    bounding_box([380, c2], width: 100, height: 20) do
      font_size 11
      text "#{@bank.to_s}" #comercio tabla
    end

    bounding_box([480, c2], width: 100, height: 20) do
      font_size 11
      text "$#{@amo.to_s}.00" #cantidad tabla
    end


  end

end