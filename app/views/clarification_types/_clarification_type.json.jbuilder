json.extract! clarification_type, :id, :name, :created_at, :updated_at
json.url clarification_type_url(clarification_type, format: :json)
