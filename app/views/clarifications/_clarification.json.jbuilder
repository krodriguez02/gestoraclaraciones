json.extract! clarification, :id, :name, :last_name_f, :last_name_m, :card_number, :phone, :email, :account_type, :clarification_type, :payment_date, :amount, :key_autorization, :bank, :atm_type, :purchase_commerce_type, :facts, :created_at, :updated_at
json.url clarification_url(clarification, format: :json)
