Rails.application.routes.draw do

  get 'productivity/index'

  resources :clarification_types
  get 'home/mensajeAlerta'
  get 'home/par3'
  get 'home/par2'
  get 'home/par1'
  get 'carga_prosas/archivos'
  get 'clarifications/archivos'
  get 'files_clarifications/archivos'

  get 'clarifications/dynamic'

  filter :locale
  resources :clarifications
  resources :states
  resources :carga_prosas
  resources :files_clarifications
  resources :views
  resources :profiles
  resources :permissions
  resources :areas
  resources :home
  resources :users

  get '/:locale' => 'home#index'

  #devise_for :users
  devise_for :users, :controllers => {:registrations => "user/registrations", :sessions => "user/sessions"}, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_scope :user do
    authenticated :user do
      root :to => 'home#index', as: :authenticated_root
    end
    unauthenticated :user do
      root :to => 'devise/sessions#new', as: :unauthenticated_root
      resources :expired_accounts
      get "expired_accounts/new"
    end
    get "auth/logout" => "devise/sessions#destroy"
  end
end
