class CreateCargaProsas < ActiveRecord::Migration[5.0]
  def change
    create_table :carga_prosas do |t|
      t.string :name
      t.datetime :fecha
      t.integer :user_id

      t.timestamps
    end
  end
end
