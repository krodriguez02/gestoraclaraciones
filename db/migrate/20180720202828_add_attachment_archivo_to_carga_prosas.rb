class AddAttachmentArchivoToCargaProsas < ActiveRecord::Migration[5.0]
  def self.up
    change_table :carga_prosas do |t|
      t.attachment :archivo
    end
  end

  def self.down
    remove_attachment :carga_prosas, :archivo
  end
end
