class RemoveColumnsFromCargaProsa < ActiveRecord::Migration[5.0]
  def change
    remove_column :carga_prosas, :archivo_file_name
    remove_column :carga_prosas, :archivo_content_type
    remove_column :carga_prosas, :archivo_file_size
    remove_column :carga_prosas, :archivo_updated_at
  end
end
