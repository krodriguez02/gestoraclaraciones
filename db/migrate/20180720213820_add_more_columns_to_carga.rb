class AddMoreColumnsToCarga < ActiveRecord::Migration[5.0]
  def change
    add_column :carga_prosas, :name_file_size, :string
    add_column :carga_prosas, :name_updated_at, :string
  end
end
