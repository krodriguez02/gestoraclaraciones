class CreateStates < ActiveRecord::Migration[5.0]
  def change
    create_table :states do |t|
      t.integer :IdEstado
      t.string :Descripcion
      t.integer :falso_positivo
      t.string :mensajeDefault

      t.timestamps
    end
  end
end
