class RemoveColumnFromStates < ActiveRecord::Migration[5.0]
  def change
    remove_column :states, :id
  end
end
