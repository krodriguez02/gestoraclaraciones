class CreateClarifications < ActiveRecord::Migration[5.0]
  def change
    create_table :clarifications do |t|
      t.string :name
      t.string :last_name_f
      t.string :last_name_m
      t.string :card_number
      t.string :phone
      t.string :email
      t.integer :account_type
      t.integer :clarification_type
      t.string :payment_date
      t.string :amount
      t.string :key_autorization
      t.string :bank
      t.string :atm_type
      t.string :purchase_commerce_type
      t.string :facts

      t.timestamps
    end
  end
end
