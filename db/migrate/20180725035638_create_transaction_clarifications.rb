class CreateTransactionClarifications < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_clarifications do |t|
      t.integer :idClarification
      t.integer :idTransaction
      t.string :tableTTName
      t.integer :userMarked

      t.timestamps
    end
  end
end
