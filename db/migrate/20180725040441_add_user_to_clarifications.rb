class AddUserToClarifications < ActiveRecord::Migration[5.0]
  def change
    add_column :clarifications, :user_id, :integer
  end
end
