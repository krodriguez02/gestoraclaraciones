class AddFolioToClarifications < ActiveRecord::Migration[5.0]
  def change
    add_column :clarifications, :folio, :string
  end
end
