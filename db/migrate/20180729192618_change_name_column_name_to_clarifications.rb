class ChangeNameColumnNameToClarifications < ActiveRecord::Migration[5.0]
  def change
    rename_column :clarifications, :name, :client_name
  end
end
