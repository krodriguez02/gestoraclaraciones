class CreateFilesClarifications < ActiveRecord::Migration[5.0]
  def change
    create_table :files_clarifications do |t|
      t.string :name
      t.integer :user_id
      t.integer :idClarification

      t.timestamps
    end
  end
end
