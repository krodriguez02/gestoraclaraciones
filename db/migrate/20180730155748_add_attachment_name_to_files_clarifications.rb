class AddAttachmentNameToFilesClarifications < ActiveRecord::Migration[5.0]
  def self.up
    change_table :files_clarifications do |t|
      t.attachment :name
    end
  end

  def self.down
    remove_attachment :files_clarifications, :name
  end
end
