class AddTransactionDetailToTransactionClarification < ActiveRecord::Migration[5.0]
  def change
    add_column :transaction_clarifications, :fecha_hora, :string
    add_column :transaction_clarifications, :numero_tarjeta, :string
    add_column :transaction_clarifications, :comercio, :string
    add_column :transaction_clarifications, :importe1, :string
    add_column :transaction_clarifications, :importe2, :string
    add_column :transaction_clarifications, :referencia23, :string
  end
end
