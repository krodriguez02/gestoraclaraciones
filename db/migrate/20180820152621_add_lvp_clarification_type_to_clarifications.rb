class AddLvpClarificationTypeToClarifications < ActiveRecord::Migration[5.0]
  def change
    add_column :clarifications, :clarification_type_id, :integer
  end
end
