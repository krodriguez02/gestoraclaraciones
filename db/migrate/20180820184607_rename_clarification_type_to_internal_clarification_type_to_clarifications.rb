class RenameClarificationTypeToInternalClarificationTypeToClarifications < ActiveRecord::Migration[5.0]
  def change
    rename_column :clarifications, :clarification_type, :internal_clarification_type
  end
end
