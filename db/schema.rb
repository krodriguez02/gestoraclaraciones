# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180906053402) do

  create_table "areas", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "area_id"
  end

  create_table "bins", force: :cascade do |t|
    t.string   "bin"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "carga_prosas", force: :cascade do |t|
    t.string   "name"
    t.datetime "fecha"
    t.integer  "user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "name_file_name"
    t.string   "name_content_type"
    t.string   "name_file_size"
    t.string   "name_updated_at"
  end

  create_table "clarification_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clarifications", force: :cascade do |t|
    t.string   "client_name"
    t.string   "last_name_f"
    t.string   "last_name_m"
    t.string   "card_number"
    t.string   "phone"
    t.string   "email"
    t.integer  "account_type"
    t.integer  "internal_clarification_type"
    t.string   "payment_date"
    t.string   "amount"
    t.string   "key_autorization"
    t.string   "bank"
    t.string   "atm_type"
    t.string   "purchase_commerce_type"
    t.string   "facts"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "user_id"
    t.string   "folio"
    t.integer  "state_id"
    t.string   "celphone"
    t.integer  "clarification_type_id"
  end

  create_table "files_clarifications", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "idClarification"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "name_file_name"
    t.string   "name_content_type"
    t.bigint   "name_file_size"
    t.datetime "name_updated_at"
  end

  create_table "old_passwords", force: :cascade do |t|
    t.string   "encrypted_password",       null: false
    t.string   "password_archivable_type", null: false
    t.integer  "password_archivable_id",   null: false
    t.datetime "created_at"
    t.string   "password_salt"
    t.index ["password_archivable_type", "password_archivable_id"], name: "index_password_archivable"
  end

  create_table "permissions", force: :cascade do |t|
    t.integer  "view_id"
    t.string   "view_name"
    t.integer  "crear"
    t.integer  "editar"
    t.integer  "leer"
    t.integer  "eliminar"
    t.integer  "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "name"
    t.integer  "flag"
    t.integer  "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "states", id: false, force: :cascade do |t|
    t.integer  "IdEstado"
    t.string   "Descripcion"
    t.string   "mensajeDefault"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "transaction_clarifications", force: :cascade do |t|
    t.integer  "idClarification"
    t.integer  "idTransaction"
    t.string   "tableTTName"
    t.integer  "userMarked"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "fecha_hora"
    t.string   "numero_tarjeta"
    t.string   "comercio"
    t.string   "importe1"
    t.string   "importe2"
    t.string   "referencia23"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                            default: "", null: false
    t.string   "encrypted_password",               default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                    default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                  default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "password_changed_at"
    t.string   "unique_session_id",      limit: 1
    t.datetime "last_activity_at"
    t.datetime "expired_at"
    t.string   "user_name"
    t.string   "name"
    t.string   "last_name"
    t.date     "birthday"
    t.string   "boss_name"
    t.string   "phone"
    t.string   "ext"
    t.integer  "profile_id"
    t.string   "area_id"
    t.string   "employment"
    t.integer  "activo"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["user_name"], name: "index_users_on_user_name", unique: true
  end

  create_table "views", force: :cascade do |t|
    t.string   "name"
    t.integer  "crear"
    t.integer  "editar"
    t.integer  "eliminar"
    t.integer  "leer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
