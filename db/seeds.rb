# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
# ##################### VISTAS BASE ######################
# vista = View.new
# vista.name = "Home"
# vista.crear = 0
# vista.editar = 0
# vista.eliminar = 0
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "Productivity"
# vista.crear = 0
# vista.editar = 0
# vista.eliminar = 0
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "Clarifications"
# vista.crear = 1
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "ProsaUpload"
# vista.crear = 1
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "Users"
# vista.crear = 0
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "Profiles"
# vista.crear = 1
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "Areas"
# vista.crear = 1
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "States"
# vista.crear = 1
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
# vista = View.new
# vista.name = "ClarificationTypes"
# vista.crear = 1
# vista.editar = 1
# vista.eliminar = 1
# vista.leer = 1
# vista.save
#
#
# ##################### AREA CENTRAL ######################
# area = Area.new
# area.name = "Central"
# area.save
#
# ##################### PERFIL ADMINISTRADOR CENTRAL ######################
# perfil = Profile.new
# perfil.name = 'Administrator Central'
# perfil.area_id = 1
# perfil.flag = 0
# perfil.save
#
# permiso = Permission.new
# permiso.view_id = 1
# permiso.view_name = View.find(1).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = 2
# permiso.eliminar = nil
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 2
# permiso.view_name = View.find(2).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = 2
# permiso.eliminar = nil
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 3
# permiso.view_name = View.find(3).name
# permiso.crear = 8
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 4
# permiso.view_name = View.find(4).name
# permiso.crear = 8
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 5
# permiso.view_name = View.find(5).name
# permiso.crear = nil
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 6
# permiso.view_name = View.find(6).name
# permiso.crear = 8
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 7
# permiso.view_name = View.find(7).name
# permiso.crear = 8
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 8
# permiso.view_name = View.find(8).name
# permiso.crear = 8
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 9
# permiso.view_name = View.find(9).name
# permiso.crear = 8
# permiso.editar = 4
# permiso.leer = 2
# permiso.eliminar = 1
# permiso.profile_id = 1
# permiso.save
#
# ##################### PERFIL Default CENTRAL ######################
# perfil = Profile.new
# perfil.name = 'Default Central'
# perfil.area_id = 1
# perfil.flag = 1
# perfil.save
#
# permiso = Permission.new
# permiso.view_id = 1
# permiso.view_name = View.find(1).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = 2
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 2
# permiso.view_name = View.find(2).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = 2
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 3
# permiso.view_name = View.find(3).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 4
# permiso.view_name = View.find(4).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 5
# permiso.view_name = View.find(5).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 6
# permiso.view_name = View.find(6).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 7
# permiso.view_name = View.find(7).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 8
# permiso.view_name = View.find(8).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# permiso = Permission.new
# permiso.view_id = 9
# permiso.view_name = View.find(9).name
# permiso.crear = nil
# permiso.editar = nil
# permiso.leer = nil
# permiso.eliminar = nil
# permiso.profile_id = 2
# permiso.save
#
# ##################### ESTADOS POR DEFAULT GESTOR DE ACLARACIONES ######################
#
# estado = State.new
# estado.IdEstado = 1
# estado.Descripcion = "No atendido"
# estado.mensajeDefault = "Estado default Gestor de Aclaraciones Liverpool"
# estado.save
#
# estado = State.new
# estado.IdEstado = 2
# estado.Descripcion = "En progreso"
# estado.mensajeDefault = "La aclaración está en progreso de atención -- Gestor de Aclaraciones Livepool"
# estado.save
#
# estado = State.new
# estado.IdEstado = 3
# estado.Descripcion = "Atendido"
# estado.mensajeDefault = "La aclaración se ha atendido -- Gestor de Aclaraciones Liverpool"
# estado.save
#
# ##################### TIPO DE ACLARACIÓN LIVERPOOL DEFAULT ######################
#
# type = ClarificationType.new
# type.name = "No asignado"
# type.save
#
# ##################### BINES ######################
#
# bin = Bin.new
# bin.bin = "417849"
# bin.name = "LPC7"
# bin.save
#
# bin = Bin.new
# bin.bin = "30000"
# bin.name = "DILISA"
# bin.save
#
# bin = Bin.new
# bin.bin = "130000"
# bin.name = "DILISA"
# bin.save
#
# bin = Bin.new
# bin.bin = "330000"
# bin.name = "DILISA"
# bin.save
#
# bin = Bin.new
# bin.bin = "530000"
# bin.name = "DILISA"
# bin.save
#
# bin = Bin.new
# bin.bin = "630000"
# bin.name = "DILISA"
# bin.save
#
# bin = Bin.new
# bin.bin = "230000"
# bin.name = "DILISA"
# bin.save
#
# bin = Bin.new
# bin.bin = "210000"
# bin.name = "Suburbia dep."
# bin.save
#
# bin = Bin.new
# bin.bin = "140000"
# bin.name = "Suburbia dep."
# bin.save
#
# bin = Bin.new
# bin.bin = "640000"
# bin.name = "Suburbia dep."
# bin.save
#
# bin = Bin.new
# bin.bin = "405067"
# bin.name = "Suburbia VISA"
# bin.save