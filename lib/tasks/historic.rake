namespace :historic do
  task id_tran: :environment do
    begin
      puts green('------------------------- Comienza alteración de histórico Key Monitor -------------------------')
      puts green(Time.now.to_s)

      puts 'Conectándose a base transaccional'
      ActiveRecord::Base.establish_connection(
          adapter: "sqlserver",
          timeout: 0,
          host: "172.16.201.148",
          pool: "1433",
          username: "admkeym",
          password: "Liverpool2",
          database: "KMNADM"
      )

      ActiveRecord::Base.connection

      puts 'Conectado'
      @table_names = ActiveRecord::Base.connection.tables

      tts = Array.new
      @table_names.each do |tbn|
        if tbn[0..1] == "TT"
          tts.push(tbn)

          if !(ActiveRecord::Base.connection.column_exists?(tbn, 'ID_TRAN'))
            puts 'Alterando tabla ' + tbn.to_s
            ActiveRecord::Migration.add_column(tbn, :ID_TRAN, :integer)

            sql = "Select Codigo_Aprov from #{tbn}"
            Timeout::timeout(0) {@registros = ActiveRecord::Base::connection.exec_query(sql)}

            puts 'Agregando auto incrementales a la tabla ' + tbn.to_s

            codigos = Array.new
            @cont = 0
            if !@registros.nil? && @registros.present?
              @registros.each do |reg|
                @cont += 1

                sin_id = "Select ID_Tran from #{tbn} where Codigo_Aprov = '#{reg['Codigo_Aprov']}'"
                Timeout::timeout(0) {@sin_id = ActiveRecord::Base::connection.exec_query(sin_id)}

                if !@sin_id.nil? && @sin_id.present?
                  @sin_id.each do |sid|
                    if sid['ID_TRAN'].nil? || sid['ID_TRAN'] == ""
                      if !(codigos.include? reg['Codigo_Aprov'])
                        codigos.push(reg['Codigo_Aprov'])
                        update = "Update #{tbn} set ID_TRAN = #{@cont} where Codigo_Aprov = '#{reg['Codigo_Aprov']}'"
                        Timeout::timeout(0) {@registros = ActiveRecord::Base::connection.exec_query(update)}

                        puts @cont.to_s
                      end
                    end
                  end
                end
              end
            end

            puts 'Registros actualizados'
          end
        end
      end

      ActiveRecord::Base.establish_connection(
          adapter: "sqlserver",
          timeout: 0,
          host: "172.16.201.148",
          pool: "1433",
          username: "admkeym",
          password: "Liverpool2",
          database: "KSGestorAclaraciones"
      )

      puts green('-------------------- Fin del proceso --------------------')
      puts green(Time.now.to_s)
    rescue => e
      puts red('ERROR ON ADD ID_TRAN TO KEY MONITOR HISTORIC: ' + Time.now.to_s)
      puts red(e.to_s)

      logger = Logger.new('log/historic_idtran.log')
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def magenta(mytext)
    ; "\e[35m#{mytext}\e[0m"
  end
end
