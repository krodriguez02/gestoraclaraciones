namespace :probando do

  task ref23: :environment do
    puts 'Se va a verificar la existencia de la columna referencia 23'
    @now = Time.now.to_time.to_s
    puts "--------------------* EMPIEZA #{@now} *--------------------"
    puts 'Se obtiene la fecha actual'

    hoy = Time.now.to_date.strftime('%Y%m%d') + '0101'
    @hoy = 'TT' + hoy
    puts 'Fecha Actual:'
    puts @hoy
    seis = 6.months.ago.to_date.strftime('%Y%m%d') + '0101'
    @seis = 'TT' + seis
    puts 'Hace seis meses:'
    puts @seis

    #query1 = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '[#{@hoy}-#{@seis}]%'"
    query1 = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME >= '#{@seis}' AND TABLE_NAME < '#{@hoy}'"
    puts query1
    Timeout::timeout(0) {@tts = ActiveRecord::Base::connection.exec_query(query1)}
    puts 'Conteo de Tablas: '
    puts @tts.length

    cont = 0
    @tts.each do |n|
      @tn = n['TABLE_NAME']
      if @tn.start_with?('TT20')
        #if @tn.start_with?('sta')
        puts 'Tabla: '
        @bu = n['TABLE_NAME']
        puts cont += 1
        puts @bu

        col = "select * from #{@bu}"
        @col = ActiveRecord::Base::connection.exec_query(col)

        # Se busca que la columna referencia23 exista
        @ref23 = ActiveRecord::Base.connection.column_exists?(:id, @bu)
        puts 'Si es true es que si existe la columna referencia23: '
        puts @ref23
        puts 'El resultado de la búsqueda es: ' + @ref23.to_s


        if @ref23 == false
          puts 'No existe la columna'
        else
          puts 'Si existe la columna'
        end

      end

    end
    puts "--------------------* TERMINA #{@now} *--------------------"

  end

end