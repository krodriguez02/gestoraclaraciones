require 'test_helper'

class CargaProsasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @carga_prosa = carga_prosas(:one)
  end

  test "should get index" do
    get carga_prosas_url
    assert_response :success
  end

  test "should get new" do
    get new_carga_prosa_url
    assert_response :success
  end

  test "should create carga_prosa" do
    assert_difference('CargaProsa.count') do
      post carga_prosas_url, params: { carga_prosa: { fecha: @carga_prosa.fecha, name: @carga_prosa.name, user_id: @carga_prosa.user_id } }
    end

    assert_redirected_to carga_prosa_url(CargaProsa.last)
  end

  test "should show carga_prosa" do
    get carga_prosa_url(@carga_prosa)
    assert_response :success
  end

  test "should get edit" do
    get edit_carga_prosa_url(@carga_prosa)
    assert_response :success
  end

  test "should update carga_prosa" do
    patch carga_prosa_url(@carga_prosa), params: { carga_prosa: { fecha: @carga_prosa.fecha, name: @carga_prosa.name, user_id: @carga_prosa.user_id } }
    assert_redirected_to carga_prosa_url(@carga_prosa)
  end

  test "should destroy carga_prosa" do
    assert_difference('CargaProsa.count', -1) do
      delete carga_prosa_url(@carga_prosa)
    end

    assert_redirected_to carga_prosas_url
  end
end
