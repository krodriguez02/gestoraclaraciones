require 'test_helper'

class ClarificationTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @clarification_type = clarification_types(:one)
  end

  test "should get index" do
    get clarification_types_url
    assert_response :success
  end

  test "should get new" do
    get new_clarification_type_url
    assert_response :success
  end

  test "should create clarification_type" do
    assert_difference('ClarificationType.count') do
      post clarification_types_url, params: { clarification_type: { name: @clarification_type.name } }
    end

    assert_redirected_to clarification_type_url(ClarificationType.last)
  end

  test "should show clarification_type" do
    get clarification_type_url(@clarification_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_clarification_type_url(@clarification_type)
    assert_response :success
  end

  test "should update clarification_type" do
    patch clarification_type_url(@clarification_type), params: { clarification_type: { name: @clarification_type.name } }
    assert_redirected_to clarification_type_url(@clarification_type)
  end

  test "should destroy clarification_type" do
    assert_difference('ClarificationType.count', -1) do
      delete clarification_type_url(@clarification_type)
    end

    assert_redirected_to clarification_types_url
  end
end
