require 'test_helper'

class ClarificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @clarification = clarifications(:one)
  end

  test "should get index" do
    get clarifications_url
    assert_response :success
  end

  test "should get new" do
    get new_clarification_url
    assert_response :success
  end

  test "should create clarification" do
    assert_difference('Clarification.count') do
      post clarifications_url, params: { clarification: { account_type: @clarification.account_type, amount: @clarification.amount, atm_type: @clarification.atm_type, bank: @clarification.bank, card_number: @clarification.card_number, clarification_type: @clarification.clarification_type, email: @clarification.email, facts: @clarification.facts, key_autorization: @clarification.key_autorization, last_name_f: @clarification.last_name_f, last_name_m: @clarification.last_name_m, name: @clarification.name, payment_date: @clarification.payment_date, phone: @clarification.phone, purchase_commerce_type: @clarification.purchase_commerce_type } }
    end

    assert_redirected_to clarification_url(Clarification.last)
  end

  test "should show clarification" do
    get clarification_url(@clarification)
    assert_response :success
  end

  test "should get edit" do
    get edit_clarification_url(@clarification)
    assert_response :success
  end

  test "should update clarification" do
    patch clarification_url(@clarification), params: { clarification: { account_type: @clarification.account_type, amount: @clarification.amount, atm_type: @clarification.atm_type, bank: @clarification.bank, card_number: @clarification.card_number, clarification_type: @clarification.clarification_type, email: @clarification.email, facts: @clarification.facts, key_autorization: @clarification.key_autorization, last_name_f: @clarification.last_name_f, last_name_m: @clarification.last_name_m, name: @clarification.name, payment_date: @clarification.payment_date, phone: @clarification.phone, purchase_commerce_type: @clarification.purchase_commerce_type } }
    assert_redirected_to clarification_url(@clarification)
  end

  test "should destroy clarification" do
    assert_difference('Clarification.count', -1) do
      delete clarification_url(@clarification)
    end

    assert_redirected_to clarifications_url
  end
end
