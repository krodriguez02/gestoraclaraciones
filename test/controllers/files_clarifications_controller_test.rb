require 'test_helper'

class FilesClarificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @files_clarification = files_clarifications(:one)
  end

  test "should get index" do
    get files_clarifications_url
    assert_response :success
  end

  test "should get new" do
    get new_files_clarification_url
    assert_response :success
  end

  test "should create files_clarification" do
    assert_difference('FilesClarification.count') do
      post files_clarifications_url, params: { files_clarification: { idClarification: @files_clarification.idClarification, name: @files_clarification.name, user_id: @files_clarification.user_id } }
    end

    assert_redirected_to files_clarification_url(FilesClarification.last)
  end

  test "should show files_clarification" do
    get files_clarification_url(@files_clarification)
    assert_response :success
  end

  test "should get edit" do
    get edit_files_clarification_url(@files_clarification)
    assert_response :success
  end

  test "should update files_clarification" do
    patch files_clarification_url(@files_clarification), params: { files_clarification: { idClarification: @files_clarification.idClarification, name: @files_clarification.name, user_id: @files_clarification.user_id } }
    assert_redirected_to files_clarification_url(@files_clarification)
  end

  test "should destroy files_clarification" do
    assert_difference('FilesClarification.count', -1) do
      delete files_clarification_url(@files_clarification)
    end

    assert_redirected_to files_clarifications_url
  end
end
