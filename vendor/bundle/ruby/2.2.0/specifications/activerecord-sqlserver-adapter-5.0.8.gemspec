# -*- encoding: utf-8 -*-
# stub: activerecord-sqlserver-adapter 5.0.8 ruby lib

Gem::Specification.new do |s|
  s.name = "activerecord-sqlserver-adapter"
  s.version = "5.0.8"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Ken Collins", "Anna Carey", "Will Bond", "Murray Steele", "Shawn Balestracci", "Joe Rafaniello", "Tom Ward"]
  s.date = "2017-12-30"
  s.description = "ActiveRecord SQL Server Adapter. SQL Server 2012 and upward."
  s.email = ["ken@metaskills.net", "will@wbond.net"]
  s.homepage = "http://github.com/rails-sqlserver/activerecord-sqlserver-adapter"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.5.2"
  s.summary = "ActiveRecord SQL Server Adapter."

  s.installed_by_version = "2.4.5.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activerecord>, ["~> 5.0.0"])
      s.add_runtime_dependency(%q<tiny_tds>, [">= 0"])
    else
      s.add_dependency(%q<activerecord>, ["~> 5.0.0"])
      s.add_dependency(%q<tiny_tds>, [">= 0"])
    end
  else
    s.add_dependency(%q<activerecord>, ["~> 5.0.0"])
    s.add_dependency(%q<tiny_tds>, [">= 0"])
  end
end
