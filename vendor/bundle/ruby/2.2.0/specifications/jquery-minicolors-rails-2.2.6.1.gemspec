# -*- encoding: utf-8 -*-
# stub: jquery-minicolors-rails 2.2.6.1 ruby lib

Gem::Specification.new do |s|
  s.name = "jquery-minicolors-rails"
  s.version = "2.2.6.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Kostiantyn Kahanskyi"]
  s.date = "2018-04-15"
  s.description = "This gem embeddes the jQuery colorpicker in the Rails asset pipeline."
  s.email = ["kostiantyn.kahanskyi@googlemail.com"]
  s.homepage = "https://github.com/kostia/jquery-minicolors-rails"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.5.2"
  s.summary = "This gem embeddes the jQuery colorpicker in the Rails asset pipeline."

  s.installed_by_version = "2.4.5.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, [">= 3.2.8"])
      s.add_runtime_dependency(%q<jquery-rails>, [">= 0"])
      s.add_development_dependency(%q<multi_json>, [">= 0"])
      s.add_development_dependency(%q<pry>, [">= 0"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_development_dependency(%q<rspec-rails>, [">= 0"])
      s.add_development_dependency(%q<simple_form>, [">= 0"])
      s.add_development_dependency(%q<webrat>, [">= 0"])
    else
      s.add_dependency(%q<rails>, [">= 3.2.8"])
      s.add_dependency(%q<jquery-rails>, [">= 0"])
      s.add_dependency(%q<multi_json>, [">= 0"])
      s.add_dependency(%q<pry>, [">= 0"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<rspec-rails>, [">= 0"])
      s.add_dependency(%q<simple_form>, [">= 0"])
      s.add_dependency(%q<webrat>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails>, [">= 3.2.8"])
    s.add_dependency(%q<jquery-rails>, [">= 0"])
    s.add_dependency(%q<multi_json>, [">= 0"])
    s.add_dependency(%q<pry>, [">= 0"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<rspec-rails>, [">= 0"])
    s.add_dependency(%q<simple_form>, [">= 0"])
    s.add_dependency(%q<webrat>, [">= 0"])
  end
end
