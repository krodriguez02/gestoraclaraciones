# -*- encoding: utf-8 -*-
# stub: devise_security_extension 0.10.0 ruby lib

Gem::Specification.new do |s|
  s.name = "devise_security_extension"
  s.version = "0.10.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Marco Scholl", "Alexander Dreher"]
  s.date = "2018-09-10"
  s.description = "An enterprise security extension for devise, trying to meet industrial standard security demands for web applications."
  s.email = "team@phatworx.de"
  s.files = [".document", ".gitignore", ".rubocop.yml", ".travis.yml", "Gemfile", "Gemfile.lock", "LICENSE.txt", "README.md", "Rakefile", "app/controllers/devise/paranoid_verification_code_controller.rb", "app/controllers/devise/password_expired_controller.rb", "app/views/devise/paranoid_verification_code/show.html.erb", "app/views/devise/password_expired/show.html.erb", "config/locales/de.yml", "config/locales/en.yml", "config/locales/es.yml", "config/locales/it.yml", "devise_security_extension.gemspec", "lib/devise_security_extension.rb", "lib/devise_security_extension/controllers/helpers.rb", "lib/devise_security_extension/hooks/expirable.rb", "lib/devise_security_extension/hooks/paranoid_verification.rb", "lib/devise_security_extension/hooks/password_expirable.rb", "lib/devise_security_extension/hooks/session_limitable.rb", "lib/devise_security_extension/models/database_authenticatable_patch.rb", "lib/devise_security_extension/models/expirable.rb", "lib/devise_security_extension/models/old_password.rb", "lib/devise_security_extension/models/paranoid_verification.rb", "lib/devise_security_extension/models/password_archivable.rb", "lib/devise_security_extension/models/password_expirable.rb", "lib/devise_security_extension/models/secure_validatable.rb", "lib/devise_security_extension/models/security_questionable.rb", "lib/devise_security_extension/models/session_limitable.rb", "lib/devise_security_extension/orm/active_record.rb", "lib/devise_security_extension/patches.rb", "lib/devise_security_extension/patches/confirmations_controller_captcha.rb", "lib/devise_security_extension/patches/confirmations_controller_security_question.rb", "lib/devise_security_extension/patches/controller_captcha.rb", "lib/devise_security_extension/patches/controller_security_question.rb", "lib/devise_security_extension/patches/passwords_controller_captcha.rb", "lib/devise_security_extension/patches/passwords_controller_security_question.rb", "lib/devise_security_extension/patches/registrations_controller_captcha.rb", "lib/devise_security_extension/patches/sessions_controller_captcha.rb", "lib/devise_security_extension/patches/unlocks_controller_captcha.rb", "lib/devise_security_extension/patches/unlocks_controller_security_question.rb", "lib/devise_security_extension/rails.rb", "lib/devise_security_extension/routes.rb", "lib/devise_security_extension/schema.rb", "lib/devise_security_extension/version.rb", "lib/generators/devise_security_extension/install_generator.rb", "lib/generators/templates/devise_security_extension.rb", "test/dummy/Rakefile", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/controllers/captcha/sessions_controller.rb", "test/dummy/app/controllers/foos_controller.rb", "test/dummy/app/controllers/security_question/unlocks_controller.rb", "test/dummy/app/models/.gitkeep", "test/dummy/app/models/captcha_user.rb", "test/dummy/app/models/secure_user.rb", "test/dummy/app/models/security_question_user.rb", "test/dummy/app/models/user.rb", "test/dummy/app/views/foos/index.html.erb", "test/dummy/config.ru", "test/dummy/config/application.rb", "test/dummy/config/boot.rb", "test/dummy/config/database.yml", "test/dummy/config/environment.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/initializers/devise.rb", "test/dummy/config/initializers/migration_class.rb", "test/dummy/config/routes.rb", "test/dummy/config/secrets.yml", "test/dummy/db/migrate/20120508165529_create_tables.rb", "test/dummy/db/migrate/20150402165590_add_verification_columns.rb", "test/dummy/db/migrate/20150407162345_add_verification_attempt_column.rb", "test/dummy/db/migrate/20160320162345_add_security_questions_fields.rb", "test/test_captcha_controller.rb", "test/test_helper.rb", "test/test_install_generator.rb", "test/test_paranoid_verification.rb", "test/test_password_archivable.rb", "test/test_password_expirable.rb", "test/test_password_expired_controller.rb", "test/test_secure_validatable.rb", "test/test_security_question_controller.rb"]
  s.homepage = "https://github.com/phatworx/devise_security_extension"
  s.licenses = ["MIT"]
  s.required_ruby_version = Gem::Requirement.new(">= 2.1.0")
  s.rubyforge_project = "devise_security_extension"
  s.rubygems_version = "2.5.1"
  s.summary = "Security extension for devise"
  s.test_files = ["test/dummy/Rakefile", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/controllers/captcha/sessions_controller.rb", "test/dummy/app/controllers/foos_controller.rb", "test/dummy/app/controllers/security_question/unlocks_controller.rb", "test/dummy/app/models/.gitkeep", "test/dummy/app/models/captcha_user.rb", "test/dummy/app/models/secure_user.rb", "test/dummy/app/models/security_question_user.rb", "test/dummy/app/models/user.rb", "test/dummy/app/views/foos/index.html.erb", "test/dummy/config.ru", "test/dummy/config/application.rb", "test/dummy/config/boot.rb", "test/dummy/config/database.yml", "test/dummy/config/environment.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/initializers/devise.rb", "test/dummy/config/initializers/migration_class.rb", "test/dummy/config/routes.rb", "test/dummy/config/secrets.yml", "test/dummy/db/migrate/20120508165529_create_tables.rb", "test/dummy/db/migrate/20150402165590_add_verification_columns.rb", "test/dummy/db/migrate/20150407162345_add_verification_attempt_column.rb", "test/dummy/db/migrate/20160320162345_add_security_questions_fields.rb", "test/test_captcha_controller.rb", "test/test_helper.rb", "test/test_install_generator.rb", "test/test_paranoid_verification.rb", "test/test_password_archivable.rb", "test/test_password_expirable.rb", "test/test_password_expired_controller.rb", "test/test_secure_validatable.rb", "test/test_security_question_controller.rb"]

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<railties>, ["< 6.0", ">= 3.2.6"])
      s.add_runtime_dependency(%q<devise>, ["< 5.0", ">= 3.0.0"])
      s.add_development_dependency(%q<bundler>, ["< 2.0", ">= 1.3.0"])
      s.add_development_dependency(%q<sqlite3>, ["~> 1.3.10"])
      s.add_development_dependency(%q<rubocop>, ["~> 0"])
      s.add_development_dependency(%q<minitest>, [">= 0"])
      s.add_development_dependency(%q<easy_captcha>, ["~> 0"])
      s.add_development_dependency(%q<rails_email_validator>, ["~> 0"])
      s.add_development_dependency(%q<coveralls>, [">= 0"])
    else
      s.add_dependency(%q<railties>, ["< 6.0", ">= 3.2.6"])
      s.add_dependency(%q<devise>, ["< 5.0", ">= 3.0.0"])
      s.add_dependency(%q<bundler>, ["< 2.0", ">= 1.3.0"])
      s.add_dependency(%q<sqlite3>, ["~> 1.3.10"])
      s.add_dependency(%q<rubocop>, ["~> 0"])
      s.add_dependency(%q<minitest>, [">= 0"])
      s.add_dependency(%q<easy_captcha>, ["~> 0"])
      s.add_dependency(%q<rails_email_validator>, ["~> 0"])
      s.add_dependency(%q<coveralls>, [">= 0"])
    end
  else
    s.add_dependency(%q<railties>, ["< 6.0", ">= 3.2.6"])
    s.add_dependency(%q<devise>, ["< 5.0", ">= 3.0.0"])
    s.add_dependency(%q<bundler>, ["< 2.0", ">= 1.3.0"])
    s.add_dependency(%q<sqlite3>, ["~> 1.3.10"])
    s.add_dependency(%q<rubocop>, ["~> 0"])
    s.add_dependency(%q<minitest>, [">= 0"])
    s.add_dependency(%q<easy_captcha>, ["~> 0"])
    s.add_dependency(%q<rails_email_validator>, ["~> 0"])
    s.add_dependency(%q<coveralls>, [">= 0"])
  end
end
